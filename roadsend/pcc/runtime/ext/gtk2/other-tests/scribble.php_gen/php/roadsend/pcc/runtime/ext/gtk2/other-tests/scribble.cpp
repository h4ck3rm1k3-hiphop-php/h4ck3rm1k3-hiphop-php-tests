
#include <php/roadsend/pcc/runtime/ext/gtk2/other-tests/scribble.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/gtk2/other-tests/scribble.php line 6 */
Variant c_scribble::os_get(const char *s, int64 hash) {
  return c_gtkwindow::os_get(s, hash);
}
Variant &c_scribble::os_lval(const char *s, int64 hash) {
  return c_gtkwindow::os_lval(s, hash);
}
void c_scribble::o_get(ArrayElementVec &props) const {
  props.push_back(NEW(ArrayElement)("size_group", m_size_group.isReferenced() ? ref(m_size_group) : m_size_group));
  props.push_back(NEW(ArrayElement)("pixmap", m_pixmap.isReferenced() ? ref(m_pixmap) : m_pixmap));
  c_gtkwindow::o_get(props);
}
bool c_scribble::o_exists(CStrRef s, int64 hash) const {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_EXISTS_STRING(0x01F201D3C627E0F4LL, size_group, 10);
      break;
    case 1:
      HASH_EXISTS_STRING(0x4EBE6820955A77B5LL, pixmap, 6);
      break;
    default:
      break;
  }
  return c_gtkwindow::o_exists(s, hash);
}
Variant c_scribble::o_get(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x01F201D3C627E0F4LL, m_size_group,
                         size_group, 10);
      break;
    case 1:
      HASH_RETURN_STRING(0x4EBE6820955A77B5LL, m_pixmap,
                         pixmap, 6);
      break;
    default:
      break;
  }
  return c_gtkwindow::o_get(s, hash);
}
Variant c_scribble::o_set(CStrRef s, int64 hash, CVarRef v,bool forInit /* = false */) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_SET_STRING(0x01F201D3C627E0F4LL, m_size_group,
                      size_group, 10);
      break;
    case 1:
      HASH_SET_STRING(0x4EBE6820955A77B5LL, m_pixmap,
                      pixmap, 6);
      break;
    default:
      break;
  }
  return c_gtkwindow::o_set(s, hash, v, forInit);
}
Variant &c_scribble::o_lval(CStrRef s, int64 hash) {
  if (hash < 0) hash = hash_string(s.data(), s.length());
  switch (hash & 3) {
    case 0:
      HASH_RETURN_STRING(0x01F201D3C627E0F4LL, m_size_group,
                         size_group, 10);
      break;
    case 1:
      HASH_RETURN_STRING(0x4EBE6820955A77B5LL, m_pixmap,
                         pixmap, 6);
      break;
    default:
      break;
  }
  return c_gtkwindow::o_lval(s, hash);
}
Variant c_scribble::os_constant(const char *s) {
  return c_gtkwindow::os_constant(s);
}
IMPLEMENT_CLASS(scribble)
ObjectData *c_scribble::create(Variant v_parent //  = null
) {
  init();
  t___construct(v_parent);
  return this;
}
ObjectData *c_scribble::dynCreate(CArrRef params, bool init /* = true */) {
  if (init) {
    int count = params.size();
    if (count <= 0) return (create());
    return (create(params.rvalAt(0)));
  } else return this;
}
void c_scribble::dynConstruct(CArrRef params) {
  int count = params.size();
  if (count <= 0) (t___construct());
  (t___construct(params.rvalAt(0)));
}
ObjectData *c_scribble::cloneImpl() {
  c_scribble *obj = NEW(c_scribble)();
  cloneSet(obj);
  return obj;
}
void c_scribble::cloneSet(c_scribble *clone) {
  clone->m_size_group = m_size_group.isReferenced() ? ref(m_size_group) : m_size_group;
  clone->m_pixmap = m_pixmap.isReferenced() ? ref(m_pixmap) : m_pixmap;
  c_gtkwindow::cloneSet(clone);
}
Variant c_scribble::o_invoke(const char *s, CArrRef params, int64 hash, bool fatal) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GUARD(0x42AFEE2A2D0EF462LL, configure_event) {
        return (t_configure_event(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 4:
      HASH_GUARD(0x1E0760FD9AB0E064LL, expose_event) {
        return (t_expose_event(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 5:
      HASH_GUARD(0x7BF3FFC487D5DE15LL, motion_notify_event) {
        return (t_motion_notify_event(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 14:
      HASH_GUARD(0x2AE0AE4F4444516ELL, button_press_event) {
        return (t_button_press_event(params.rvalAt(0), params.rvalAt(1)));
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        int count = params.size();
        if (count <= 0) return (t___construct(), null);
        return (t___construct(params.rvalAt(0)), null);
      }
      break;
    default:
      break;
  }
  return c_gtkwindow::o_invoke(s, params, hash, fatal);
}
Variant c_scribble::o_invoke_few_args(const char *s, int64 hash, int count, CVarRef a0, CVarRef a1, CVarRef a2, CVarRef a3, CVarRef a4, CVarRef a5) {
  if (hash < 0) hash = hash_string_i(s);
  switch (hash & 15) {
    case 2:
      HASH_GUARD(0x42AFEE2A2D0EF462LL, configure_event) {
        return (t_configure_event(a0, a1));
      }
      break;
    case 4:
      HASH_GUARD(0x1E0760FD9AB0E064LL, expose_event) {
        return (t_expose_event(a0, a1));
      }
      break;
    case 5:
      HASH_GUARD(0x7BF3FFC487D5DE15LL, motion_notify_event) {
        return (t_motion_notify_event(a0, a1));
      }
      break;
    case 14:
      HASH_GUARD(0x2AE0AE4F4444516ELL, button_press_event) {
        return (t_button_press_event(a0, a1));
      }
      break;
    case 15:
      HASH_GUARD(0x0D31D0AC229C615FLL, __construct) {
        if (count <= 0) return (t___construct(), null);
        return (t___construct(a0), null);
      }
      break;
    default:
      break;
  }
  return c_gtkwindow::o_invoke_few_args(s, hash, count, a0, a1, a2, a3, a4, a5);
}
Variant c_scribble::os_invoke(const char *c, const char *s, CArrRef params, int64 hash, bool fatal) {
  return c_gtkwindow::os_invoke(c, s, params, hash, fatal);
}
Variant cw_scribble$os_get(const char *s) {
  return c_scribble::os_get(s, -1);
}
Variant &cw_scribble$os_lval(const char *s) {
  return c_scribble::os_lval(s, -1);
}
Variant cw_scribble$os_constant(const char *s) {
  return c_scribble::os_constant(s);
}
Variant cw_scribble$os_invoke(const char *c, const char *s, CArrRef params, bool fatal /* = true */) {
  return c_scribble::os_invoke(c, s, params, -1, fatal);
}
void c_scribble::init() {
  c_gtkwindow::init();
  m_size_group = null;
  m_pixmap = null;
}
/* SRC: roadsend/pcc/runtime/ext/gtk2/other-tests/scribble.php line 12 */
void c_scribble::t___construct(Variant v_parent //  = null
) {
  INSTANCE_METHOD_INJECTION(Scribble, Scribble::__construct);
  bool oldInCtor = gasInCtor(true);
  DECLARE_GLOBAL_VARIABLES(g);
  LINE(14,throw_fatal("unknown class gtkwindow", ((void*)NULL)));
  if (toBoolean((silenceInc(), silenceDec(toBoolean(g->GV(framework)))))) {
    {
      gasInCtor(oldInCtor);
      return;
    }
  }
  if (toBoolean(v_parent)) LINE(21,o_root_invoke_few_args("set_screen", 0x241ECAB7C477C368LL, 1, v_parent.o_invoke_few_args("get_screen", 0x323ADF90E7F255DDLL, 0)));
  else LINE(23,o_root_invoke_few_args("connect_simple", 0x7EA731EFEB16D515LL, 2, "destroy", Array(ArrayInit(2).set(0, "gtk").set(1, "main_quit").create())));
  LINE(25,o_root_invoke_few_args("set_title", 0x67B0FD2A129B593ALL, 1, "Scribble"));
  LINE(26,o_root_invoke_few_args("set_position", 0x17D027C2B74A20CBLL, 1, throw_fatal("unknown class constant gtk::WIN_POS_CENTER")));
  LINE(27,o_root_invoke_few_args("set_default_size", 0x146EBA1471FE3028LL, 2, -1LL, -1LL));
  LINE(28,o_root_invoke_few_args("set_border_width", 0x50380C0EEA02D4B3LL, 1, 8LL));
  LINE(30,o_root_invoke_few_args("add", 0x15D34462FC79458BLL, 1, t___create_box()));
  LINE(31,o_root_invoke_few_args("show_all", 0x6D2BC3CAAF1533BDLL, 0));
  gasInCtor(oldInCtor);
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk2/other-tests/scribble.php line 36 */
Object c_scribble::t___create_box() {
  INSTANCE_METHOD_INJECTION(Scribble, Scribble::__create_box);
  Variant eo_0;
  Object v_vbox;
  Variant v_drawing_area;

  (v_vbox = LINE(38,create_object("gtkvbox", Array())));
  LINE(39,v_vbox->o_invoke_few_args("show", 0x35121557DC0EAE15LL, 0));
  (v_drawing_area = LINE(41,create_object("gtkdrawingarea", Array())));
  LINE(42,v_drawing_area.o_invoke_few_args("set_size_request", 0x5A59BE07D4E90F49LL, 2, 300LL, 300LL));
  LINE(43,v_vbox->o_invoke_few_args("pack_start", 0x2D6BDE948B85D9F0LL, 1, v_drawing_area));
  LINE(46,v_drawing_area.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "expose_event", Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "expose_event").create())));
  LINE(47,v_drawing_area.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "configure_event", Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "configure_event").create())));
  LINE(49,v_drawing_area.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "motion_notify_event", Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "motion_notify_event").create())));
  LINE(50,v_drawing_area.o_invoke_few_args("connect", 0x7032C660AD16D7FELL, 2, "button_press_event", Array(ArrayInit(2).set(0, ((Object)(this))).set(1, "button_press_event").create())));
  (assignCallTemp(eo_0, toObject(v_drawing_area)),LINE(56,eo_0.o_invoke_few_args("set_events", 0x7FC3726CF670E586LL, 1, bitwise_or(bitwise_or(bitwise_or(bitwise_or(throw_fatal("unknown class constant gdk::EXPOSURE_MASK"), throw_fatal("unknown class constant gdk::LEAVE_NOTIFY_MASK")), throw_fatal("unknown class constant gdk::BUTTON_PRESS_MASK")), throw_fatal("unknown class constant gdk::POINTER_MOTION_MASK")), throw_fatal("unknown class constant gdk::POINTER_MOTION_HINT_MASK")))));
  return v_vbox;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk2/other-tests/scribble.php line 63 */
bool c_scribble::t_configure_event(CVarRef v_widget, CVarRef v_event) {
  INSTANCE_METHOD_INJECTION(Scribble, Scribble::configure_event);
  Variant eo_0;
  (m_pixmap = LINE(68,create_object("gdkpixmap", Array(ArrayInit(4).set(0, toObject(v_widget).o_get("window", 0x6B178C4E4A2617E2LL)).set(1, toObject(v_widget).o_get("allocation", 0x08A76C0C7C76DE59LL).o_get("width", 0x79CE5ED9259796ADLL)).set(2, toObject(v_widget).o_get("allocation", 0x08A76C0C7C76DE59LL).o_get("height", 0x674F0FBB080B0779LL)).set(3, -1LL).create()))));
  (assignCallTemp(eo_0, toObject(m_pixmap)),LINE(72,eo_0.o_invoke_few_args("draw_rectangle", 0x251AACC4879A1DCELL, 6, toObject(v_widget).o_get("style", 0x040EF9119982696ALL).o_lval("white_gc", 0x06A1D59F7A0AE817LL), true, 0LL, 0LL, toObject(v_widget).o_get("allocation", 0x08A76C0C7C76DE59LL).o_lval("width", 0x79CE5ED9259796ADLL), toObject(v_widget).o_get("allocation", 0x08A76C0C7C76DE59LL).o_lval("height", 0x674F0FBB080B0779LL))));
  return true;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk2/other-tests/scribble.php line 77 */
bool c_scribble::t_expose_event(Variant v_widget, CVarRef v_event) {
  INSTANCE_METHOD_INJECTION(Scribble, Scribble::expose_event);
  Variant eo_0;
  (assignCallTemp(eo_0, toObject(v_widget.o_get("window", 0x6B178C4E4A2617E2LL))),LINE(83,eo_0.o_invoke("draw_drawable", Array(ArrayInit(8).set(0, ref(lval(lval(v_widget.o_lval("style", 0x040EF9119982696ALL)).o_lval("fg_gc", 0x4F103A9D3FE599E3LL)).refvalAt(v_widget.o_get("state", 0x5C84C80672BA5E22LL)))).set(1, ref(m_pixmap)).set(2, ref(toObject(v_event).o_get("area", 0x19E4B1F66A7A41EDLL).o_lval("x", 0x04BFC205E59FA416LL))).set(3, ref(toObject(v_event).o_get("area", 0x19E4B1F66A7A41EDLL).o_lval("y", 0x4F56B733A4DFC78ALL))).set(4, ref(toObject(v_event).o_get("area", 0x19E4B1F66A7A41EDLL).o_lval("x", 0x04BFC205E59FA416LL))).set(5, ref(toObject(v_event).o_get("area", 0x19E4B1F66A7A41EDLL).o_lval("y", 0x4F56B733A4DFC78ALL))).set(6, ref(toObject(v_event).o_get("area", 0x19E4B1F66A7A41EDLL).o_lval("width", 0x79CE5ED9259796ADLL))).set(7, ref(toObject(v_event).o_get("area", 0x19E4B1F66A7A41EDLL).o_lval("height", 0x674F0FBB080B0779LL))).create()), 0x072C6997135ACABFLL)));
  return false;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk2/other-tests/scribble.php line 90 */
bool c_scribble::t_button_press_event(CVarRef v_widget, CVarRef v_event) {
  INSTANCE_METHOD_INJECTION(Scribble, Scribble::button_press_event);
  if (equal(toObject(v_event).o_get("button", 0x614CB098CEBB2764LL), 1LL) && toBoolean(m_pixmap)) {
    LINE(93,t_draw_brush(v_widget, toInt64(toObject(v_event).o_get("x", 0x04BFC205E59FA416LL)), toInt64(toObject(v_event).o_get("y", 0x4F56B733A4DFC78ALL))));
  }
  return true;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk2/other-tests/scribble.php line 101 */
bool c_scribble::t_motion_notify_event(CVarRef v_widget, CVarRef v_event) {
  INSTANCE_METHOD_INJECTION(Scribble, Scribble::motion_notify_event);
  Variant v_window;
  Variant v_pointer;
  Variant v_x;
  Variant v_y;
  Variant v_state;

  (v_window = toObject(v_event).o_get("window", 0x6B178C4E4A2617E2LL));
  (v_pointer = LINE(104,v_window.o_invoke_few_args("get_pointer", 0x6581902F9171507BLL, 0)));
  (v_x = v_pointer.rvalAt(0LL, 0x77CFA1EEF01BCA90LL));
  (v_y = v_pointer.rvalAt(1LL, 0x5BCA7C69B794F8CELL));
  (v_state = v_pointer.rvalAt(2LL, 0x486AFCC090D5F98CLL));
  if ((toBoolean(bitwise_and(v_state, throw_fatal("unknown class constant gdk::BUTTON1_MASK")))) && toBoolean(m_pixmap)) {
    LINE(110,t_draw_brush(v_widget, v_x, v_y));
  }
  return true;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/gtk2/other-tests/scribble.php line 118 */
void c_scribble::t_draw_brush(CVarRef v_widget, CVarRef v_x, CVarRef v_y) {
  INSTANCE_METHOD_INJECTION(Scribble, Scribble::draw_brush);
  LINE(120,m_pixmap.o_invoke("draw_arc", Array(ArrayInit(8).set(0, ref(toObject(v_widget).o_get("style", 0x040EF9119982696ALL).o_lval("black_gc", 0x7C0401670AEB6690LL))).set(1, true).set(2, v_x - 4LL).set(3, v_y - 4LL).set(4, 8LL).set(5, 8LL).set(6, 0LL).set(7, 23040LL).create()), 0x34F55761570F0F13LL));
  LINE(121,toObject(v_widget)->o_invoke_few_args("queue_draw_area", 0x0F5A6F744EFF880ELL, 4, v_x - 4LL, v_y - 4LL, 8LL, 8LL));
} /* function */
Object co_scribble(CArrRef params, bool init /* = true */) {
  return Object(p_scribble(NEW(c_scribble)())->dynCreate(params, init));
}
Variant pm_php$roadsend$pcc$runtime$ext$gtk2$other_tests$scribble_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/gtk2/other-tests/scribble.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$gtk2$other_tests$scribble_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  (g->GV(class) = "Scribble");
  (g->GV(description) = "Scribble lets you draw paintings with your mouse");
  if (!(toBoolean((silenceInc(), silenceDec(toBoolean(g->GV(framework))))))) {
    ((Object)(LINE(130,p_scribble(p_scribble(NEWOBJ(c_scribble)())->create()))));
    LINE(131,throw_fatal("unknown class gtk", ((void*)NULL)));
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
