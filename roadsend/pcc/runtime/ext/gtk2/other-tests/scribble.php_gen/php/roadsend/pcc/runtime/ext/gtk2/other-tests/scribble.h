
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_gtk2_other_tests_scribble_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_gtk2_other_tests_scribble_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/gtk2/other-tests/scribble.fw.h>

// Declarations
#include <cls/scribble.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$gtk2$other_tests$scribble_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_scribble(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_gtk2_other_tests_scribble_h__
