
#ifndef __GENERATED_cls_scribble_h__
#define __GENERATED_cls_scribble_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/runtime/ext/gtk2/other-tests/scribble.php line 6 */
class c_scribble : virtual public ObjectData {
  BEGIN_CLASS_MAP(scribble)
  END_CLASS_MAP(scribble)
  DECLARE_CLASS(scribble, Scribble, gtkwindow)
  void init();
  public: Variant m_size_group;
  public: Variant m_pixmap;
  public: void t___construct(Variant v_parent = null);
  public: ObjectData *create(Variant v_parent = null);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
  public: void dynConstruct(CArrRef params);
  public: Object t___create_box();
  public: bool t_configure_event(CVarRef v_widget, CVarRef v_event);
  public: bool t_expose_event(Variant v_widget, CVarRef v_event);
  public: bool t_button_press_event(CVarRef v_widget, CVarRef v_event);
  public: bool t_motion_notify_event(CVarRef v_widget, CVarRef v_event);
  public: void t_draw_brush(CVarRef v_widget, CVarRef v_x, CVarRef v_y);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_scribble_h__
