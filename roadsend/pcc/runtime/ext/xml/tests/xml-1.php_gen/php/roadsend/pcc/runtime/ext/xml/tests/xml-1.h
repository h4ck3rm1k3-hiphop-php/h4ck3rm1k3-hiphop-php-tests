
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_xml_tests_xml_1_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_xml_tests_xml_1_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/xml/tests/xml-1.fw.h>

// Declarations
#include <cls/xmlparser.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$xml$tests$xml_1_php(bool incOnce = false, LVariableTable* variables = NULL);
Object co_xmlparser(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_xml_tests_xml_1_h__
