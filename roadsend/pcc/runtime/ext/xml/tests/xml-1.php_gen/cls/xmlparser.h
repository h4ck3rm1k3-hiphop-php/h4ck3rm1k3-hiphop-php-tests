
#ifndef __GENERATED_cls_xmlparser_h__
#define __GENERATED_cls_xmlparser_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/runtime/ext/xml/tests/xml-1.php line 7 */
class c_xmlparser : virtual public ObjectData {
  BEGIN_CLASS_MAP(xmlparser)
  END_CLASS_MAP(xmlparser)
  DECLARE_CLASS(xmlparser, xmlparser, ObjectData)
  void init();
  public: void t_startelement(CVarRef v_parser, CVarRef v_name, CVarRef v_attrs);
  public: void t_endelement(CVarRef v_parser, CVarRef v_name);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_xmlparser_h__
