
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_xml_tests_xml_5_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_xml_tests_xml_5_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/xml/tests/xml-5.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_endelement(CVarRef v_parser, CVarRef v_name);
void f_startelement(CVarRef v_parser, CVarRef v_name, CVarRef v_attrs);
Variant pm_php$roadsend$pcc$runtime$ext$xml$tests$xml_5_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_xml_tests_xml_5_h__
