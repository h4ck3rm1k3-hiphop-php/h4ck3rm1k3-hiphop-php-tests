
#ifndef __GENERATED_cls_aminoacid_h__
#define __GENERATED_cls_aminoacid_h__


namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* SRC: roadsend/pcc/runtime/ext/xml/tests/xml-4.php line 3 */
class c_aminoacid : virtual public ObjectData {
  BEGIN_CLASS_MAP(aminoacid)
  END_CLASS_MAP(aminoacid)
  DECLARE_CLASS(aminoacid, AminoAcid, ObjectData)
  void init();
  public: String m_name;
  public: String m_symbol;
  public: String m_code;
  public: String m_type;
  public: void t_aminoacid(CVarRef v_aa);
  public: ObjectData *create(CVarRef v_aa);
  public: ObjectData *dynCreate(CArrRef params, bool init = true);
};

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_cls_aminoacid_h__
