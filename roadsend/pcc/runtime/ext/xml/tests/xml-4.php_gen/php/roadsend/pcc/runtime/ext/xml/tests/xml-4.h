
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_xml_tests_xml_4_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_xml_tests_xml_4_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/xml/tests/xml-4.fw.h>

// Declarations
#include <cls/aminoacid.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

p_aminoacid f_parsemol(CVarRef v_mvalues);
Variant pm_php$roadsend$pcc$runtime$ext$xml$tests$xml_4_php(bool incOnce = false, LVariableTable* variables = NULL);
Array f_readdatabase(CVarRef v_filename);
Object co_aminoacid(CArrRef params, bool init = true);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_xml_tests_xml_4_h__
