
#include <php/roadsend/pcc/runtime/ext/xml/tests/xml-3.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$xml$tests$xml_3_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/xml/tests/xml-3.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$xml$tests$xml_3_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_simple __attribute__((__unused__)) = (variables != gVariables) ? variables->get("simple") : g->GV(simple);
  Variant &v_p __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p") : g->GV(p);
  Variant &v_vals __attribute__((__unused__)) = (variables != gVariables) ? variables->get("vals") : g->GV(vals);
  Variant &v_index __attribute__((__unused__)) = (variables != gVariables) ? variables->get("index") : g->GV(index);
  Variant &v_p2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("p2") : g->GV(p2);

  (v_simple = "<para>cdata test<note>simple note</note><note> sample  two\n\nend here</note><open test='test'></open></para>");
  (v_p = LINE(7,x_xml_parser_create()));
  LINE(8,x_xml_parse_into_struct(toObject(v_p), toString(v_simple), ref(v_vals), ref(v_index)));
  LINE(9,x_xml_parser_free(toObject(v_p)));
  echo("Index array\n");
  LINE(11,x_print_r(v_index));
  echo("\nVals array\n");
  LINE(13,x_print_r(v_vals));
  (v_p2 = LINE(15,x_xml_parser_create()));
  LINE(16,x_xml_parser_set_option(toObject(v_p2), toInt32(1LL) /* XML_OPTION_CASE_FOLDING */, 0LL));
  LINE(17,x_xml_parser_set_option(toObject(v_p2), toInt32(4LL) /* XML_OPTION_SKIP_WHITE */, 0LL));
  LINE(18,x_xml_parse_into_struct(toObject(v_p2), toString(v_simple), ref(v_vals), ref(v_index)));
  LINE(19,x_xml_parser_free(toObject(v_p2)));
  echo("Index array\n");
  LINE(21,x_print_r(v_index));
  echo("\nVals array\n");
  LINE(23,x_print_r(v_vals));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
