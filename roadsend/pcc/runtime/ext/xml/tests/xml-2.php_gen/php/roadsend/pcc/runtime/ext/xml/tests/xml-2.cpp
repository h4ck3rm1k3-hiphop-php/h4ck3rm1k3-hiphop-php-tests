
#include <php/roadsend/pcc/runtime/ext/xml/tests/xml-2.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/xml/tests/xml-2.php line 29 */
void f_notation(CVarRef v_parser, CVarRef v_name, CVarRef v_base, CVarRef v_sysid, CVarRef v_pubid) {
  FUNCTION_INJECTION(notation);
  Variant v_names;

  print(concat("notation: [", LINE(30,concat6(toString(v_names), "] [", toString(v_sysid), "] [", toString(v_pubid), "]\n"))));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/xml/tests/xml-2.php line 33 */
void f_pi_h(CVarRef v_parser, CVarRef v_target, CVarRef v_data) {
  FUNCTION_INJECTION(pi_h);
  print(LINE(34,concat5("pi: [", toString(v_target), "] [", toString(v_data), "]")));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/xml/tests/xml-2.php line 17 */
void f_cdata(CVarRef v_parser, CVarRef v_data) {
  FUNCTION_INJECTION(cdata);
  print(LINE(18,concat3("cdata: [", toString(v_data), "]\n")));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/xml/tests/xml-2.php line 25 */
void f_ext_ent(CVarRef v_parser, CVarRef v_names, CVarRef v_base, CVarRef v_sysid, CVarRef v_pubid) {
  FUNCTION_INJECTION(ext_ent);
  print(concat("external: [", LINE(26,concat6(toString(v_names), "] [", toString(v_sysid), "] [", toString(v_pubid), "]\n"))));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/xml/tests/xml-2.php line 37 */
void f_unparsed(CVarRef v_parser, CVarRef v_name, CVarRef v_base, CVarRef v_sysid, CVarRef v_pubid, CVarRef v_notation) {
  FUNCTION_INJECTION(unparsed);
  Variant v_names;

  print(concat_rev(LINE(38,concat6(toString(v_sysid), "] [", toString(v_pubid), "] [", toString(v_notation), "]\n")), concat3("unparsed: [", toString(v_names), "] [")));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/xml/tests/xml-2.php line 21 */
void f_default_h(CVarRef v_parser, CVarRef v_data) {
  FUNCTION_INJECTION(default_h);
  print(LINE(22,concat3("default: [", toString(v_data), "]\n")));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/xml/tests/xml-2.php line 13 */
void f_endelement(CVarRef v_parser, CVarRef v_name) {
  FUNCTION_INJECTION(endElement);
  print(LINE(14,concat3("end: [", toString(v_name), "]\n")));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/xml/tests/xml-2.php line 8 */
void f_startelement(CVarRef v_parser, CVarRef v_name, CVarRef v_attrs) {
  FUNCTION_INJECTION(startElement);
  print(LINE(9,concat3("start: [", toString(v_name), "]\n")));
  LINE(10,x_print_r(v_attrs));
} /* function */
Variant i_notation(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x6071F0A4D7F152C9LL, notation) {
    return (f_notation(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_pi_h(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x2FB64D1F23CFC5EALL, pi_h) {
    return (f_pi_h(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_cdata(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x0A70D397C0570F56LL, cdata) {
    return (f_cdata(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_ext_ent(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x339EC43DC58DC7B8LL, ext_ent) {
    return (f_ext_ent(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_unparsed(CArrRef params) {
  return (f_unparsed(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2), params.rvalAt(3), params.rvalAt(4), params.rvalAt(5)), null);
}
Variant i_default_h(CArrRef params) {
  return (f_default_h(params.rvalAt(0), params.rvalAt(1)), null);
}
Variant i_endelement(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x78A7A5D96EF2F4AFLL, endelement) {
    return (f_endelement(params.rvalAt(0), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_startelement(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x55BFFBE5717EFD62LL, startelement) {
    return (f_startelement(params.rvalAt(0), params.rvalAt(1), params.rvalAt(2)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$pcc$runtime$ext$xml$tests$xml_2_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/xml/tests/xml-2.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$xml$tests$xml_2_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_file __attribute__((__unused__)) = (variables != gVariables) ? variables->get("file") : g->GV(file);
  Variant &v_xml_parser __attribute__((__unused__)) = (variables != gVariables) ? variables->get("xml_parser") : g->GV(xml_parser);
  Variant &v_fp __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fp") : g->GV(fp);
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);

  (v_file = concat(toString(LINE(5,x_getenv("PCC_HOME"))), "/runtime/ext/xml/tests/data2.xml"));
  echo(LINE(6,concat3("working with ", toString(v_file), "\n")));
  (v_xml_parser = LINE(41,x_xml_parser_create()));
  LINE(43,x_xml_set_element_handler(toObject(v_xml_parser), "startElement", "endElement"));
  LINE(44,x_xml_set_character_data_handler(toObject(v_xml_parser), "cdata"));
  LINE(45,x_xml_set_default_handler(toObject(v_xml_parser), "default_h"));
  LINE(46,x_xml_set_external_entity_ref_handler(toObject(v_xml_parser), "ext_ent"));
  LINE(47,x_xml_set_notation_decl_handler(toObject(v_xml_parser), "notation"));
  LINE(48,x_xml_set_processing_instruction_handler(toObject(v_xml_parser), "pi_h"));
  LINE(49,x_xml_set_unparsed_entity_decl_handler(toObject(v_xml_parser), "unparsed"));
  if (!((toBoolean((v_fp = LINE(51,x_fopen(toString(v_file), "r"))))))) {
    f_exit("could not open XML input");
  }
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_data = LINE(55,x_fread(toObject(v_fp), 4096LL))))) {
      LOOP_COUNTER_CHECK(1);
      {
        if (!(toBoolean(LINE(56,(assignCallTemp(eo_0, toObject(v_xml_parser)),assignCallTemp(eo_1, toString(v_data)),assignCallTemp(eo_2, x_feof(toObject(v_fp))),x_xml_parse(eo_0, eo_1, eo_2)))))) {
          f_exit(LINE(59,(assignCallTemp(eo_1, LINE(58,x_xml_error_string(x_xml_get_error_code(toObject(v_xml_parser))))),assignCallTemp(eo_2, LINE(59,x_xml_get_current_line_number(toObject(v_xml_parser)))),x_sprintf(3, "XML error: %s at line %d", Array(ArrayInit(2).set(0, eo_1).set(1, eo_2).create())))));
        }
      }
    }
  }
  LINE(62,x_xml_parser_free(toObject(v_xml_parser)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
