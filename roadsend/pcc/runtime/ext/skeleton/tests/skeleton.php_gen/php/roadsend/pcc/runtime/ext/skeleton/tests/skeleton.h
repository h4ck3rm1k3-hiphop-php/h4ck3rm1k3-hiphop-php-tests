
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_skeleton_tests_skeleton_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_skeleton_tests_skeleton_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/skeleton/tests/skeleton.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$skeleton$tests$skeleton_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_skeleton_tests_skeleton_h__
