
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_pcre_tests_pcre_7_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_pcre_tests_pcre_7_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/pcre/tests/pcre-7.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$pcre$tests$pcre_7_php(bool incOnce = false, LVariableTable* variables = NULL);
String f_next_year(CVarRef v_matches);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_pcre_tests_pcre_7_h__
