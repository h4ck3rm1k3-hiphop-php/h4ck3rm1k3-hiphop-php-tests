
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

GlobalVariables::~GlobalVariables() {}
Variant &GlobalVariables::getImpl(CStrRef str, int64 hash) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char *s __attribute__((__unused__)) = str.data();
  if (hash < 0) hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_RETURN(0x50645ABB5EE07801LL, g->gv__POST,
                  _POST);
      break;
    case 2:
      HASH_RETURN(0x2027864469AD4382LL, g->GV(string),
                  string);
      break;
    case 3:
      HASH_RETURN(0x6649184C41356B03LL, g->gv_HTTP_RAW_POST_DATA,
                  HTTP_RAW_POST_DATA);
      HASH_RETURN(0x11A5375142A75D03LL, g->gv_http_response_header,
                  http_response_header);
      HASH_RETURN(0x4D3DDB0569EA1683LL, g->GV(subj),
                  subj);
      HASH_RETURN(0x2A28A0084DD3A743LL, g->GV(text),
                  text);
      break;
    case 6:
      HASH_RETURN(0x10EA7DC57768F8C6LL, g->gv_argv,
                  argv);
      HASH_RETURN(0x2D4BA4F8CFD63BC6LL, g->GV(replace),
                  replace);
      break;
    case 7:
      HASH_RETURN(0x089A564752853F87LL, g->GV(html_body),
                  html_body);
      break;
    case 8:
      HASH_RETURN(0x489ED1A5055B40C8LL, g->GV(docText),
                  docText);
      break;
    case 9:
      HASH_RETURN(0x0759FB4517508949LL, g->gv__GET,
                  _GET);
      break;
    case 14:
      HASH_RETURN(0x516FBD36FC674A0ELL, g->gv__FILES,
                  _FILES);
      break;
    case 16:
      HASH_RETURN(0x7320B4E3FF243290LL, g->gv__ENV,
                  _ENV);
      break;
    case 17:
      HASH_RETURN(0x29DFC3A6DC027BD1LL, g->gv__SESSION,
                  _SESSION);
      HASH_RETURN(0x1B6F6CF52DED3C51LL, g->GV(newtext),
                  newtext);
      break;
    case 19:
      HASH_RETURN(0x596A642EB89EED13LL, g->gv_argc,
                  argc);
      break;
    case 22:
      HASH_RETURN(0x18539A8FA8882D56LL, g->GV(replacement),
                  replacement);
      break;
    case 32:
      HASH_RETURN(0x029C0A5F2FB04920LL, g->GV(pattern),
                  pattern);
      break;
    case 34:
      HASH_RETURN(0x09CA7E19F9E99762LL, g->GV(replacements),
                  replacements);
      break;
    case 35:
      HASH_RETURN(0x3760929554A51DA3LL, g->gv__COOKIE,
                  _COOKIE);
      break;
    case 39:
      HASH_RETURN(0x6973DC663442E667LL, g->GV(SELF),
                  SELF);
      break;
    case 46:
      HASH_RETURN(0x14297F74B68B58EELL, g->gv__SERVER,
                  _SERVER);
      break;
    case 56:
      HASH_RETURN(0x1BCB2650D8888738LL, g->GV(patterns),
                  patterns);
      break;
    case 57:
      HASH_RETURN(0x018CF25A9A4C77F9LL, g->GV(sVars),
                  sVars);
      break;
    case 63:
      HASH_RETURN(0x1F878FB806A18D3FLL, g->gv__REQUEST,
                  _REQUEST);
      break;
    default:
      break;
  }
  return lvalAt(str, hash);
}

///////////////////////////////////////////////////////////////////////////////
}
