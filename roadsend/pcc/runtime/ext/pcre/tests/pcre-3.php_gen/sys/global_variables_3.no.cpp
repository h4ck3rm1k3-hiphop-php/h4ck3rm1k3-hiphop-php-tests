
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 63) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 2:
      HASH_INDEX(0x2027864469AD4382LL, string, 13);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      HASH_INDEX(0x4D3DDB0569EA1683LL, subj, 12);
      HASH_INDEX(0x2A28A0084DD3A743LL, text, 23);
      break;
    case 6:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x2D4BA4F8CFD63BC6LL, replace, 18);
      break;
    case 7:
      HASH_INDEX(0x089A564752853F87LL, html_body, 19);
      break;
    case 8:
      HASH_INDEX(0x489ED1A5055B40C8LL, docText, 20);
      break;
    case 9:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 16:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      break;
    case 17:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      HASH_INDEX(0x1B6F6CF52DED3C51LL, newtext, 24);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      break;
    case 22:
      HASH_INDEX(0x18539A8FA8882D56LL, replacement, 15);
      break;
    case 32:
      HASH_INDEX(0x029C0A5F2FB04920LL, pattern, 14);
      break;
    case 34:
      HASH_INDEX(0x09CA7E19F9E99762LL, replacements, 17);
      break;
    case 35:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 39:
      HASH_INDEX(0x6973DC663442E667LL, SELF, 21);
      break;
    case 46:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 56:
      HASH_INDEX(0x1BCB2650D8888738LL, patterns, 16);
      break;
    case 57:
      HASH_INDEX(0x018CF25A9A4C77F9LL, sVars, 22);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 25) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
