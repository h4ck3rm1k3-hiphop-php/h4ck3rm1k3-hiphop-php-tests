
#include <php/roadsend/pcc/runtime/ext/pcre/tests/pcre-1.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$pcre$tests$pcre_1_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/pcre/tests/pcre-1.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$pcre$tests$pcre_1_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_s __attribute__((__unused__)) = (variables != gVariables) ? variables->get("s") : g->GV(s);
  Variant &v_m __attribute__((__unused__)) = (variables != gVariables) ? variables->get("m") : g->GV(m);
  Variant &v_matches __attribute__((__unused__)) = (variables != gVariables) ? variables->get("matches") : g->GV(matches);
  Variant &v_host __attribute__((__unused__)) = (variables != gVariables) ? variables->get("host") : g->GV(host);

  (v_s = "laz/y brown cow jumps over the sly fox");
  echo(toString(LINE(6,x_preg_match("/la[zZ]\\/y/S", toString(v_s)))));
  echo(toString(LINE(7,x_preg_match("-cow-", toString(v_s)))));
  echo(toString(LINE(8,x_preg_match("-cow-", toString(v_s)))));
  echo(toString(LINE(9,x_preg_match("    /JUMPS/i", toString(v_s)))));
  echo(toString(LINE(10,x_preg_match("/JUMPS/i", toString(v_s)))));
  echo(toString(LINE(11,x_preg_match("/goo/", toString(v_s)))));
  LINE(13,x_preg_match("/sly\\s*(.+)$/", toString(v_s), ref(v_m)));
  LINE(14,x_print_r(v_m));
  LINE(16,x_preg_match("/the\\s*(.+)\\s(.+)$/", toString(v_s), ref(v_m), toInt32(256LL) /* PREG_OFFSET_CAPTURE */));
  LINE(17,x_print_r(v_m));
  LINE(23,x_preg_match("/^(http:\\/\\/)\?([^\\/]+)/i", "http://www.php.net/index.html", ref(v_matches)));
  (v_host = v_matches.rvalAt(2LL, 0x486AFCC090D5F98CLL));
  LINE(26,x_preg_match("/[^\\.\\/]+\\.[^\\.\\/]+$/", toString(v_host), ref(v_matches)));
  echo(LINE(27,concat3("domain name is: ", toString(v_matches.rvalAt(0LL, 0x77CFA1EEF01BCA90LL)), "\n")));
  echo(toString(LINE(30,x_preg_match("!foo!", "somefooyo"))));
  echo(toString(LINE(31,x_preg_match("#foo#", "somefooyo"))));
  echo(toString(LINE(32,x_preg_match("(foo)", "somefooyo"))));
  echo(toString(LINE(33,x_preg_match("<foo>", "somefooyo"))));
  echo(toString(LINE(34,x_preg_match("{foo}", "somefooyo"))));
  echo(toString(LINE(35,x_preg_match("[foo]", "somefooyo"))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
