
#include <php/roadsend/pcc/runtime/ext/pcre/tests/pcre-4.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$pcre$tests$pcre_4_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/pcre/tests/pcre-4.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$pcre$tests$pcre_4_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_keywords __attribute__((__unused__)) = (variables != gVariables) ? variables->get("keywords") : g->GV(keywords);
  Variant &v_str __attribute__((__unused__)) = (variables != gVariables) ? variables->get("str") : g->GV(str);
  Variant &v_chars __attribute__((__unused__)) = (variables != gVariables) ? variables->get("chars") : g->GV(chars);
  Variant &v_line __attribute__((__unused__)) = (variables != gVariables) ? variables->get("line") : g->GV(line);
  Variant &v_elements __attribute__((__unused__)) = (variables != gVariables) ? variables->get("elements") : g->GV(elements);
  Variant &v_a __attribute__((__unused__)) = (variables != gVariables) ? variables->get("a") : g->GV(a);

  LINE(3,x_print_r(x_preg_split("/,/", "this,is,a,list,of,values")));
  LINE(5,x_print_r(x_preg_split("/vjv/", "firstvjvsecondvjvthirdvjvfourth")));
  LINE(7,x_print_r(x_preg_split("/\\|/", "first|second|third|fourth", toInt32(2LL))));
  LINE(9,x_print_r(x_preg_split("/\\s+/", "one two three four five six seven eight nine ten", toInt32(5LL))));
  LINE(12,x_print_r(x_preg_split("/Y/", "no splits here man")));
  LINE(15,x_print_r(x_preg_split("/(\\-)/", "one-two-three-four-five-six-seven-eight-nine-ten", toInt32(-1LL), toInt32(2LL) /* PREG_SPLIT_DELIM_CAPTURE */)));
  LINE(20,x_print_r(x_preg_split("/(\\-)/", "one-two-three-four-five-six-seven-eight-nine-ten", toInt32(-1LL), toInt32(3LL))));
  LINE(24,x_print_r(x_preg_split("/(\\-)/", "one-two-three-four-five-six-seven-eight-nine-ten", toInt32(-1LL), toInt32(6LL))));
  LINE(30,x_print_r(x_preg_split("/(\\-)/", "one-two-three-four-five-six-seven-eight-nine-ten", toInt32(-1LL), toInt32(7LL))));
  LINE(35,x_print_r(x_preg_split("//", "one two three four five six seven eight nine ten", toInt32(-1LL), toInt32(5LL))));
  (v_keywords = LINE(41,x_preg_split("/[\\s,]+/", "hypertext language, programming")));
  LINE(42,x_print_r(v_keywords));
  (v_str = "string");
  (v_chars = LINE(46,x_preg_split("//", v_str, toInt32(-1LL), toInt32(1LL) /* PREG_SPLIT_NO_EMPTY */)));
  LINE(47,x_print_r(v_chars));
  (v_str = "hypertext language programming");
  (v_chars = LINE(51,x_preg_split("/ /", v_str, toInt32(-1LL), toInt32(4LL) /* PREG_SPLIT_OFFSET_CAPTURE */)));
  LINE(52,x_print_r(v_chars));
  (v_line = "10.0.0.2 - - [17/Mar/2003:18:03:08 +1100] \"GET /images/org_background.gif HTTP/1.0\" 200 2321 \"http://10.0.0.3/login.php\" \"Mozilla/5.0 Galeon/1.2.7 (X11; Linux i686; U;) Gecko/20021203\"");
  (v_elements = LINE(57,x_preg_split("/^(\\S+) (\\S+) (\\S+) \\[([^\\]]+)\\] \"([^\"]+)\" (\\S+) (\\S+) \"([^\"]+)\" \"([^\"]+)\"/", v_line, toInt32(-1LL), toInt32(3LL))));
  LINE(59,x_print_r(v_elements));
  (v_a = LINE(61,x_preg_split("/\\s+/", " ", toInt32(-1LL), toInt32(1LL) /* PREG_SPLIT_NO_EMPTY */)));
  LINE(62,x_var_dump(1, v_a));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
