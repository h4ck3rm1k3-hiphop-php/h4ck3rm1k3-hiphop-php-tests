
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_pcre_tests_pcre_6_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_pcre_tests_pcre_6_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/pcre/tests/pcre-6.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$pcre$tests$pcre_6_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_pcre_tests_pcre_6_h__
