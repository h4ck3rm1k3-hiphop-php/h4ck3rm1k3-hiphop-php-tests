
#include <php/roadsend/pcc/runtime/ext/pcre/tests/pcre-6.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$pcre$tests$pcre_6_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/pcre/tests/pcre-6.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$pcre$tests$pcre_6_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_array __attribute__((__unused__)) = (variables != gVariables) ? variables->get("array") : g->GV(array);

  LINE(4,x_print_r(x_preg_grep("/goo/", ScalarArrays::sa_[0])));
  (v_array = ScalarArrays::sa_[1]);
  LINE(10,x_print_r(x_preg_grep("/^(\\d+)\?\\.\\d+$/", toArray(v_array))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
