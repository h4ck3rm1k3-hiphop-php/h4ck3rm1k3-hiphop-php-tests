
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_sockets_tests_http_get_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_sockets_tests_http_get_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/sockets/tests/http-get.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$sockets$tests$http_get_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_sockets_tests_http_get_h__
