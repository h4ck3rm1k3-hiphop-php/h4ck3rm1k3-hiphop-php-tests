
#include <php/roadsend/pcc/runtime/ext/sockets/tests/http-get.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$sockets$tests$http_get_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/sockets/tests/http-get.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$sockets$tests$http_get_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_service_port __attribute__((__unused__)) = (variables != gVariables) ? variables->get("service_port") : g->GV(service_port);
  Variant &v_address __attribute__((__unused__)) = (variables != gVariables) ? variables->get("address") : g->GV(address);
  Variant &v_socket __attribute__((__unused__)) = (variables != gVariables) ? variables->get("socket") : g->GV(socket);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_addr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("addr") : g->GV(addr);
  Variant &v_port __attribute__((__unused__)) = (variables != gVariables) ? variables->get("port") : g->GV(port);
  Variant &v_in __attribute__((__unused__)) = (variables != gVariables) ? variables->get("in") : g->GV(in);
  Variant &v_out __attribute__((__unused__)) = (variables != gVariables) ? variables->get("out") : g->GV(out);

  echo("<h2>TCP/IP Connection</h2>\n");
  (v_service_port = LINE(6,x_getservbyname("www", "tcp")));
  (v_address = LINE(9,x_gethostbyname("www.roadsend.com")));
  (v_socket = LINE(12,x_socket_create(toInt32(2LL) /* AF_INET */, toInt32(1LL) /* SOCK_STREAM */, toInt32(6LL) /* SOL_TCP */)));
  if (same(v_socket, false)) {
    echo(LINE(14,(assignCallTemp(eo_1, x_socket_strerror(x_socket_last_error())),concat3("socket_create() failed: reason: ", eo_1, "\n"))));
  }
  else {
    echo("OK.\n");
  }
  echo(LINE(19,concat5("Attempting to connect to '", toString(v_address), "' on port '", toString(v_service_port), "'...")));
  (v_result = LINE(20,x_socket_connect(toObject(v_socket), toString(v_address), toInt32(v_service_port))));
  if (same(v_result, false)) {
    echo(LINE(22,(assignCallTemp(eo_1, toString(v_result)),assignCallTemp(eo_3, x_socket_strerror(x_socket_last_error(toObject(v_socket)))),concat5("socket_connect() failed.\nReason: (", eo_1, ") ", eo_3, "\n"))));
  }
  else {
    echo("OK.\n");
  }
  LINE(27,x_socket_getpeername(toObject(v_socket), ref(v_addr), ref(v_port)));
  echo(LINE(28,concat5("peer address: [", toString(v_addr), ":", toString(v_port), "]\n")));
  LINE(29,x_socket_getsockname(toObject(v_socket), ref(v_addr), ref(v_port)));
  echo(LINE(30,concat5("local address: [", toString(v_addr), ":", toString(v_port), "]\n")));
  (v_in = "GET /test.txt HTTP/1.1\r\nHost: www.roadsend.com\r\nConnection: Close\r\n\r\n");
  (v_out = "");
  echo("Sending HTTP request...");
  LINE(38,(assignCallTemp(eo_0, toObject(v_socket)),assignCallTemp(eo_1, toString(v_in)),assignCallTemp(eo_2, x_strlen(toString(v_in))),x_socket_write(eo_0, eo_1, eo_2)));
  echo("OK.\n");
  echo("Reading response:\n\n");
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_out = LINE(42,x_socket_read(toObject(v_socket), toInt32(2048LL)))))) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(toString(v_out));
      }
    }
  }
  echo("Closing socket...");
  LINE(47,x_socket_close(toObject(v_socket)));
  echo("OK.\n\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
