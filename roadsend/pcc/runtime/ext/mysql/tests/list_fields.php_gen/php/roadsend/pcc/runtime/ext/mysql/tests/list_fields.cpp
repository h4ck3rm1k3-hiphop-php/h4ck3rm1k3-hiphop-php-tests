
#include <php/roadsend/pcc/runtime/ext/mysql/tests/list_fields.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$mysql$tests$list_fields_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/mysql/tests/list_fields.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$mysql$tests$list_fields_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_fields __attribute__((__unused__)) = (variables != gVariables) ? variables->get("fields") : g->GV(fields);
  Variant &v_columns __attribute__((__unused__)) = (variables != gVariables) ? variables->get("columns") : g->GV(columns);
  Variant &v_i __attribute__((__unused__)) = (variables != gVariables) ? variables->get("i") : g->GV(i);

  (v_link = LINE(3,x_mysql_connect("localhost", "develUser", "d3v3lpa55")));
  (v_fields = LINE(5,x_mysql_list_fields("test", "my_table", v_link)));
  (v_columns = LINE(7,x_mysql_num_fields(v_fields)));
  {
    LOOP_COUNTER(1);
    for ((v_i = 0LL); less(v_i, v_columns); v_i++) {
      LOOP_COUNTER_CHECK(1);
      {
        echo(concat(toString(LINE(10,x_mysql_field_name(v_fields, toInt32(v_i)))), "\n"));
      }
    }
  }
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
