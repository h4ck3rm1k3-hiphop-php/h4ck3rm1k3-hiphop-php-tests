
#include <php/roadsend/pcc/runtime/ext/mysql/tests/mysql_escape_string.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$mysql$tests$mysql_escape_string_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/mysql/tests/mysql_escape_string.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$mysql$tests$mysql_escape_string_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_input __attribute__((__unused__)) = (variables != gVariables) ? variables->get("input") : g->GV(input);
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);

  (v_input = "Thar's cheese in them thar hole\n");
  echo(LINE(5,concat3("Normal: ", toString(v_input), "\n")));
  echo(LINE(6,(assignCallTemp(eo_1, x_mysql_escape_string(toString(v_input))),concat3("Eaten: ", eo_1, "\n"))));
  (toBoolean((v_link = LINE(8,x_mysql_connect("localhost", "develUser", "d3v3lpa55"))))) || (toBoolean(f_exit("Could not connect")));
  echo(LINE(11,(assignCallTemp(eo_1, toString(x_mysql_real_escape_string(toString(v_input), v_link))),concat3("Eaten: ", eo_1, "\n"))));
  LINE(13,x_mysql_close(v_link));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
