
#include <php/roadsend/pcc/runtime/ext/mysql/tests/mysql_field_name.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$mysql$tests$mysql_field_name_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/mysql/tests/mysql_field_name.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$mysql$tests$mysql_field_name_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_link __attribute__((__unused__)) = (variables != gVariables) ? variables->get("link") : g->GV(link);
  Variant &v_res __attribute__((__unused__)) = (variables != gVariables) ? variables->get("res") : g->GV(res);

  (toBoolean((v_link = LINE(2,x_mysql_pconnect("localhost", "develUser", "d3v3lpa55"))))) || (toBoolean(f_exit("Could not connect")));
  (toBoolean(LINE(5,x_mysql_select_db("test")))) || (toBoolean(f_exit("Could not select database")));
  (v_res = LINE(8,x_mysql_query("select * from my_table", v_link)));
  echo(concat(toString(LINE(10,x_mysql_field_name(v_res, toInt32(0LL)))), "\n"));
  echo(toString(LINE(11,x_mysql_field_name(v_res, toInt32(2LL)))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
