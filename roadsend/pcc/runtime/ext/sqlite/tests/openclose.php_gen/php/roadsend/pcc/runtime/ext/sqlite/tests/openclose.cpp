
#include <php/roadsend/pcc/runtime/ext/sqlite/tests/openclose.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$sqlite$tests$openclose_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/sqlite/tests/openclose.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$sqlite$tests$openclose_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_dbname __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dbname") : g->GV(dbname);
  Variant &v_db __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db") : g->GV(db);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_errmsg __attribute__((__unused__)) = (variables != gVariables) ? variables->get("errmsg") : g->GV(errmsg);

  LINE(3,require("s_common.inc", false, variables, "roadsend/pcc/runtime/ext/sqlite/tests/"));
  (v_db = LINE(5,invoke_failed("sqlite_open", Array(ArrayInit(2).set(0, ref(v_dbname)).set(1, 416LL).create()), 0x000000003E8C3F6FLL)));
  echo(LINE(6,(assignCallTemp(eo_1, toString(x_is_resource(v_db))),concat3("resource\? ", eo_1, "\n"))));
  (v_r = LINE(7,invoke_failed("sqlite_close", Array(ArrayInit(1).set(0, ref(v_db)).create()), 0x00000000CE90140ALL)));
  LINE(8,x_var_dump(1, v_r));
  (v_r = LINE(9,invoke_failed("sqlite_close", Array(ArrayInit(1).set(0, ref(v_db)).create()), 0x00000000CE90140ALL)));
  LINE(10,x_var_dump(1, v_r));
  (v_db = LINE(12,invoke_failed("sqlite_open", Array(ArrayInit(3).set(0, "/never-exists/this-will-fail").set(1, 438LL).set(2, ref(v_errmsg)).create()), 0x000000003E8C3F6FLL)));
  echo(LINE(13,(assignCallTemp(eo_1, toString(x_is_resource(v_db))),concat3("resource\? ", eo_1, "\n"))));
  echo(toString(LINE(15,x_substr(toString(v_errmsg), toInt32(0LL), toInt32(10LL)))));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
