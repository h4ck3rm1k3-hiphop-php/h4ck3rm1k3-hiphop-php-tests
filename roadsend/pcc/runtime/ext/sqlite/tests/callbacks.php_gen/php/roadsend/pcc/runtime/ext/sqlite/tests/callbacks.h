
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_sqlite_tests_callbacks_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_sqlite_tests_callbacks_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/sqlite/tests/callbacks.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$sqlite$tests$callbacks_php(bool incOnce = false, LVariableTable* variables = NULL);
Variant f_max_len_finalize(Variant v_context);
String f_md5_and_reverse(CVarRef v_string);
void f_max_len_step(Variant v_context, CVarRef v_string);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_sqlite_tests_callbacks_h__
