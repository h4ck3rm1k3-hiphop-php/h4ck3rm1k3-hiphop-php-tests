
#include <php/roadsend/pcc/runtime/ext/sqlite/tests/callbacks.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/sqlite/tests/callbacks.php line 68 */
Variant f_max_len_finalize(Variant v_context) {
  FUNCTION_INJECTION(max_len_finalize);
  echo(LINE(70,concat3("in finalize, context is: ", toString(v_context), "\n")));
  LINE(71,x_var_dump(1, v_context));
  return v_context;
} /* function */
/* SRC: roadsend/pcc/runtime/ext/sqlite/tests/callbacks.php line 26 */
String f_md5_and_reverse(CVarRef v_string) {
  FUNCTION_INJECTION(md5_and_reverse);
  echo(LINE(28,concat3("in callback, original string is ", toString(v_string), "\n")));
  return LINE(29,x_strrev(x_md5(toString(v_string))));
} /* function */
/* SRC: roadsend/pcc/runtime/ext/sqlite/tests/callbacks.php line 60 */
void f_max_len_step(Variant v_context, CVarRef v_string) {
  FUNCTION_INJECTION(max_len_step);
  echo(LINE(62,concat3("in max_len_step: ", toString(v_string), "\n")));
  if (more(LINE(63,x_strlen(toString(v_string))), v_context)) {
    (v_context = LINE(64,x_strlen(toString(v_string))));
  }
} /* function */
Variant i_max_len_finalize(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x4188971A9C73D6B3LL, max_len_finalize) {
    return (f_max_len_finalize(ref(const_cast<Array&>(params).lvalAt(0))));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_md5_and_reverse(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x20A09EC50AC11E8CLL, md5_and_reverse) {
    return (f_md5_and_reverse(params.rvalAt(0)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant i_max_len_step(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x3AEE9B341F2DFDB2LL, max_len_step) {
    return (f_max_len_step(ref(const_cast<Array&>(params).lvalAt(0)), params.rvalAt(1)), null);
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$pcc$runtime$ext$sqlite$tests$callbacks_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/sqlite/tests/callbacks.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$sqlite$tests$callbacks_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_db __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db") : g->GV(db);
  Variant &v_rows __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rows") : g->GV(rows);
  Variant &v_sql __attribute__((__unused__)) = (variables != gVariables) ? variables->get("sql") : g->GV(sql);
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);
  Variant &v_str __attribute__((__unused__)) = (variables != gVariables) ? variables->get("str") : g->GV(str);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);

  LINE(3,require("s_common.inc", false, variables, "roadsend/pcc/runtime/ext/sqlite/tests/"));
  (v_db = LINE(5,invoke_failed("makedb", Array(), 0x000000009D69F93DLL)));
  LINE(8,invoke_failed("sqlite_create_function", Array(ArrayInit(4).set(0, ref(v_db)).set(1, "md5rev").set(2, "md5_and_reverse").set(3, 1LL).create()), 0x000000006E229833LL));
  LINE(9,invoke_failed("sqlite_create_aggregate", Array(ArrayInit(4).set(0, ref(v_db)).set(1, "max_len").set(2, "max_len_step").set(3, "max_len_finalize").create()), 0x00000000DBE0DD25LL));
  (v_rows = LINE(13,invoke_failed("sqlite_array_query", Array(ArrayInit(2).set(0, ref(v_db)).set(1, "SELECT php('md5', my_string) from mytable").create()), 0x00000000013709F9LL)));
  LINE(14,x_var_dump(1, v_rows));
  (v_rows = LINE(16,invoke_failed("sqlite_array_query", Array(ArrayInit(2).set(0, ref(v_db)).set(1, "SELECT php('max', my_float, 2) from mytable").create()), 0x00000000013709F9LL)));
  LINE(17,x_var_dump(1, v_rows));
  (v_rows = LINE(20,invoke_failed("sqlite_array_query", Array(ArrayInit(2).set(0, ref(v_db)).set(1, "SELECT php('unknown', my_string) from mytable").create()), 0x00000000013709F9LL)));
  LINE(21,x_var_dump(1, v_rows));
  (v_sql = "SELECT md5rev(my_string) FROM mytable");
  (v_rows = LINE(35,invoke_failed("sqlite_array_query", Array(ArrayInit(2).set(0, ref(v_db)).set(1, ref(v_sql)).create()), 0x00000000013709F9LL)));
  LINE(36,x_var_dump(1, v_rows));
  (v_data = ScalarArrays::sa_[0]);
  LINE(54,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, ref(v_db)).set(1, "CREATE TABLE strings(a)").create()), 0x00000000ADED2A6ALL));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_data.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_str = iter3->second();
      {
        (v_str = LINE(56,invoke_failed("sqlite_escape_string", Array(ArrayInit(1).set(0, ref(v_str)).create()), 0x00000000CAE267E2LL)));
        LINE(57,(assignCallTemp(eo_0, ref(v_db)),assignCallTemp(eo_1, concat3("INSERT INTO strings VALUES ('", toString(v_str), "')")),invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x00000000ADED2A6ALL)));
      }
    }
  }
  LINE(76,x_var_dump(1, invoke_failed("sqlite_array_query", Array(ArrayInit(2).set(0, ref(v_db)).set(1, "SELECT max_len(a) from strings").create()), 0x00000000013709F9LL)));
  (v_r = LINE(79,invoke_failed("sqlite_close", Array(ArrayInit(1).set(0, ref(v_db)).create()), 0x00000000CE90140ALL)));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
