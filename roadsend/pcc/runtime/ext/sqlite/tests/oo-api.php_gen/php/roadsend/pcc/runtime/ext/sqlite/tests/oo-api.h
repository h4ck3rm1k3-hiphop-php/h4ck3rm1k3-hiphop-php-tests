
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_sqlite_tests_oo_api_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_sqlite_tests_oo_api_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/sqlite/tests/oo-api.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$sqlite$tests$oo_api_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_sqlite_tests_oo_api_h__
