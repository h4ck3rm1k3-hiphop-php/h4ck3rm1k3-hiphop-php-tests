
#include <php/roadsend/pcc/runtime/ext/sqlite/tests/oo-api.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$sqlite$tests$oo_api_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/sqlite/tests/oo-api.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$sqlite$tests$oo_api_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_obj __attribute__((__unused__)) = (variables != gVariables) ? variables->get("obj") : g->GV(obj);

  LINE(3,require("s_common.inc", false, variables, "roadsend/pcc/runtime/ext/sqlite/tests/"));
  (v_obj = LINE(5,invoke_failed("makedbobj", Array(), 0x00000000CDC883EDLL)));
  LINE(6,x_var_dump(1, v_obj));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
