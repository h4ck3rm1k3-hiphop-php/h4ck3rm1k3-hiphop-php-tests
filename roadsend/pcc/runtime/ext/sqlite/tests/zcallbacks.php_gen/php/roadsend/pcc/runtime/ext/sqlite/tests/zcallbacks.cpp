
#include <php/roadsend/pcc/runtime/ext/sqlite/tests/zcallbacks.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_SQLITE_NUM = "SQLITE_NUM";

/* preface starts */
/* preface finishes */
/* SRC: roadsend/pcc/runtime/ext/sqlite/tests/zcallbacks.php line 16 */
String f_implode_args(int num_args, Array args /* = Array() */) {
  FUNCTION_INJECTION(implode_args);
  Variant v_args;
  Variant v_sep;

  (v_args = LINE(18,func_get_args(num_args, Array(),args)));
  (v_sep = LINE(19,x_array_shift(ref(v_args))));
  return LINE(20,x_implode(v_sep, v_args));
} /* function */
Variant i_implode_args(const char *s, CArrRef params, int64 hash, bool fatal) {
  HASH_GUARD(0x7BC2FF3AAB710D40LL, implode_args) {
    int count = params.size();
    if (count <= 0) return (f_implode_args(count));
    return (f_implode_args(count,params.slice(0, count - 0, false)));
  }
  return invoke_builtin(s, params, hash, fatal);
}
Variant pm_php$roadsend$pcc$runtime$ext$sqlite$tests$zcallbacks_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/sqlite/tests/zcallbacks.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$sqlite$tests$zcallbacks_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  Variant eo_3;
  Variant eo_4;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_db __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db") : g->GV(db);
  Variant &v_data __attribute__((__unused__)) = (variables != gVariables) ? variables->get("data") : g->GV(data);
  Variant &v_row __attribute__((__unused__)) = (variables != gVariables) ? variables->get("row") : g->GV(row);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);

  LINE(3,require("s_common.inc", false, variables, "roadsend/pcc/runtime/ext/sqlite/tests/"));
  (v_db = LINE(5,invoke_failed("makedb", Array(), 0x000000009D69F93DLL)));
  (v_data = ScalarArrays::sa_[0]);
  LINE(14,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "CREATE TABLE strings(a,b)").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL));
  {
    LOOP_COUNTER(1);
    for (ArrayIterPtr iter3 = v_data.begin(); !iter3->end(); iter3->next()) {
      LOOP_COUNTER_CHECK(1);
      v_row = iter3->second();
      {
        LINE(24,(assignCallTemp(eo_0, concat(concat_rev(toString(invoke_failed("sqlite_escape_string", Array(ArrayInit(1).set(0, ref(v_row.refvalAt(1LL, 0x5BCA7C69B794F8CELL))).create()), 0x00000000CAE267E2LL)), (assignCallTemp(eo_3, toString(invoke_failed("sqlite_escape_string", Array(ArrayInit(1).set(0, ref(v_row.refvalAt(0LL, 0x77CFA1EEF01BCA90LL))).create()), 0x00000000CAE267E2LL))),concat3("INSERT INTO strings VALUES('", eo_3, "','"))), "')")),assignCallTemp(eo_1, ref(v_db)),invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, eo_0).set(1, eo_1).create()), 0x00000000ADED2A6ALL)));
      }
    }
  }
  LINE(27,invoke_failed("sqlite_create_function", Array(ArrayInit(3).set(0, ref(v_db)).set(1, "implode").set(2, "implode_args").create()), 0x000000006E229833LL));
  (v_r = LINE(29,invoke_failed("sqlite_query", Array(ArrayInit(2).set(0, "SELECT implode('-', a, b) from strings").set(1, ref(v_db)).create()), 0x00000000ADED2A6ALL)));
  LOOP_COUNTER(4);
  {
    while (toBoolean((v_row = LINE(30,invoke_failed("sqlite_fetch_array", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQLITE_NUM).create()), 0x000000002867E087LL))))) {
      LOOP_COUNTER_CHECK(4);
      {
        LINE(31,x_var_dump(1, v_row));
      }
    }
  }
  (v_r = LINE(35,invoke_failed("sqlite_close", Array(ArrayInit(1).set(0, ref(v_db)).create()), 0x00000000CE90140ALL)));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
