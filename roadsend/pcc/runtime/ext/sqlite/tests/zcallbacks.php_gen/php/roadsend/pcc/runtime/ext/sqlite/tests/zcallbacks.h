
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_sqlite_tests_zcallbacks_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_sqlite_tests_zcallbacks_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/sqlite/tests/zcallbacks.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

String f_implode_args(int num_args, Array args = Array());
Variant pm_php$roadsend$pcc$runtime$ext$sqlite$tests$zcallbacks_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_sqlite_tests_zcallbacks_h__
