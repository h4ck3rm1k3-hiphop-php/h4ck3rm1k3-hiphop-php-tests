
#include <php/roadsend/pcc/runtime/ext/sqlite/tests/exec.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$sqlite$tests$exec_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/sqlite/tests/exec.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$sqlite$tests$exec_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_db __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db") : g->GV(db);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);

  LINE(3,require("s_common.inc", false, variables, "roadsend/pcc/runtime/ext/sqlite/tests/"));
  (v_db = LINE(5,invoke_failed("makedb", Array(), 0x000000009D69F93DLL)));
  (v_r = LINE(7,invoke_failed("sqlite_close", Array(ArrayInit(1).set(0, ref(v_db)).create()), 0x00000000CE90140ALL)));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
