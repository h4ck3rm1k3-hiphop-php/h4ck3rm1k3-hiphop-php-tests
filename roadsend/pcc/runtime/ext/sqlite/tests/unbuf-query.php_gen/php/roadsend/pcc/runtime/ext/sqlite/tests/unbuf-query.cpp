
#include <php/roadsend/pcc/runtime/ext/sqlite/tests/unbuf-query.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$sqlite$tests$unbuf_query_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/sqlite/tests/unbuf-query.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$sqlite$tests$unbuf_query_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_db __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db") : g->GV(db);
  Variant &v_rh __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rh") : g->GV(rh);
  Variant &v_result __attribute__((__unused__)) = (variables != gVariables) ? variables->get("result") : g->GV(result);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);

  LINE(3,require("s_common.inc", false, variables, "roadsend/pcc/runtime/ext/sqlite/tests/"));
  (v_db = LINE(5,invoke_failed("makedb", Array(), 0x000000009D69F93DLL)));
  (v_rh = LINE(7,invoke_failed("sqlite_unbuffered_query", Array(ArrayInit(2).set(0, ref(v_db)).set(1, "SELECT * FROM mytable").create()), 0x000000006FAA6ED4LL)));
  echo(LINE(8,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("result resource\? ", eo_1, "\n"))));
  echo(LINE(9,(assignCallTemp(eo_1, toString(invoke_failed("sqlite_num_fields", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000807B2163LL))),concat3("num cols: ", eo_1, "\n"))));
  if (toBoolean(v_rh)) {
    echo("fetching results\n");
    LOOP_COUNTER(1);
    {
      while (toBoolean((v_result = LINE(14,invoke_failed("sqlite_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x000000002867E087LL))))) {
        LOOP_COUNTER_CHECK(1);
        {
          LINE(15,x_var_dump(1, v_result));
        }
      }
    }
  }
  else {
    echo(concat("bad query: ", toString(LINE(20,invoke_failed("sqlite_error_string", Array(ArrayInit(1).set(0, ref(v_db)).create()), 0x00000000B0060456LL)))));
  }
  (v_r = LINE(23,invoke_failed("sqlite_close", Array(ArrayInit(1).set(0, ref(v_db)).create()), 0x00000000CE90140ALL)));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
