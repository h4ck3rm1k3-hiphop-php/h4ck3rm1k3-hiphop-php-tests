
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_sqlite_tests_ztests_fw_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_sqlite_tests_ztests_fw_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

// Forward Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

// 1. Constants
extern const StaticString k_SQLITE_NUM;
extern const StaticString k_SQLITE_BOTH;
extern const StaticString k_SQLITE_ASSOC;


// 2. Classes

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_sqlite_tests_ztests_fw_h__
