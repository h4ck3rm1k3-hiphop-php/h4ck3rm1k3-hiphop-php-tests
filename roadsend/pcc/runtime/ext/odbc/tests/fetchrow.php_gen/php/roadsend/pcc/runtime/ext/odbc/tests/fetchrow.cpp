
#include <php/roadsend/pcc/runtime/ext/odbc/tests/fetchrow.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$odbc$tests$fetchrow_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/odbc/tests/fetchrow.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$odbc$tests$fetchrow_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_directdsn __attribute__((__unused__)) = (variables != gVariables) ? variables->get("directdsn") : g->GV(directdsn);
  Variant &v_db_user __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_user") : g->GV(db_user);
  Variant &v_db_pass __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_pass") : g->GV(db_pass);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_rh __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rh") : g->GV(rh);
  Variant &v_rows __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rows") : g->GV(rows);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_val1 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val1") : g->GV(val1);
  Variant &v_val2 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val2") : g->GV(val2);
  Variant &v_val3 __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val3") : g->GV(val3);
  Variant &v_rval __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rval") : g->GV(rval);

  LINE(3,include("connect.inc", false, variables, "roadsend/pcc/runtime/ext/odbc/tests/"));
  (v_r = LINE(5,invoke_failed("odbc_connect", Array(ArrayInit(3).set(0, ref(v_directdsn)).set(1, ref(v_db_user)).set(2, ref(v_db_pass)).create()), 0x00000000FE9537A1LL)));
  echo(LINE(7,(assignCallTemp(eo_1, toString(x_is_resource(v_r))),concat3("resource\? ", eo_1, "\n"))));
  if (!(toBoolean(v_r))) {
    echo(toString(LINE(9,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  (v_rh = LINE(13,invoke_failed("odbc_exec", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "SELECT * FROM my_table").create()), 0x00000000B7FC229FLL)));
  if (!(toBoolean(v_rh))) {
    echo("odbc_exec failed!\n");
    echo(toString(LINE(16,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    echo(toString(LINE(17,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
    f_exit(1LL);
  }
  echo(LINE(21,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("resource\? ", eo_1, "\n"))));
  (v_rows = LINE(22,invoke_failed("odbc_num_rows", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000D8DBF1DDLL)));
  echo(LINE(23,concat3("num rows: ", toString(v_rows), "\n")));
  LINE(24,x_var_dump(1, v_rows));
  echo("testing auto odbc_fetch_row\n");
  (v_val = LINE(28,invoke_failed("odbc_result", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 2LL).create()), 0x00000000C27C9E4BLL)));
  LINE(29,x_var_dump(1, v_val));
  echo("testing looped odbc_fetch_row\n");
  LOOP_COUNTER(1);
  {
    while (toBoolean(LINE(33,invoke_failed("odbc_fetch_row", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000EBCECC34LL)))) {
      LOOP_COUNTER_CHECK(1);
      {
        (v_val = LINE(34,invoke_failed("odbc_result", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 1LL).create()), 0x00000000C27C9E4BLL)));
        LINE(35,x_var_dump(1, v_val));
        (v_val1 = LINE(36,invoke_failed("odbc_result", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 4LL).create()), 0x00000000C27C9E4BLL)));
        LINE(37,x_var_dump(1, v_val1));
        (v_val2 = LINE(38,invoke_failed("odbc_result", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, "field2").create()), 0x00000000C27C9E4BLL)));
        LINE(39,x_var_dump(1, v_val2));
        (v_val3 = LINE(40,invoke_failed("odbc_result", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, "nothere").create()), 0x00000000C27C9E4BLL)));
        LINE(41,x_var_dump(1, v_val3));
      }
    }
  }
  echo("testing odbc_fetch_row with row specified\n");
  (v_rval = LINE(46,invoke_failed("odbc_fetch_row", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 2LL).create()), 0x00000000EBCECC34LL)));
  LINE(47,x_var_dump(1, v_rval));
  (v_val = LINE(48,invoke_failed("odbc_result", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 1LL).create()), 0x00000000C27C9E4BLL)));
  LINE(49,x_var_dump(1, v_val));
  (v_val1 = LINE(50,invoke_failed("odbc_result", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 4LL).create()), 0x00000000C27C9E4BLL)));
  LINE(51,x_var_dump(1, v_val1));
  (v_val2 = LINE(52,invoke_failed("odbc_result", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, "field2").create()), 0x00000000C27C9E4BLL)));
  LINE(53,x_var_dump(1, v_val2));
  (v_val3 = LINE(54,invoke_failed("odbc_result", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, "nothere").create()), 0x00000000C27C9E4BLL)));
  LINE(55,x_var_dump(1, v_val3));
  (v_rval = LINE(58,invoke_failed("odbc_fetch_row", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 200LL).create()), 0x00000000EBCECC34LL)));
  LINE(59,x_var_dump(1, v_rval));
  echo(toString(LINE(61,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
