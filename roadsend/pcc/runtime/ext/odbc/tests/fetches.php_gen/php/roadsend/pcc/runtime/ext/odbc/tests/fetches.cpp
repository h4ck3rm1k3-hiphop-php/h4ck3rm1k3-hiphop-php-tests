
#include <php/roadsend/pcc/runtime/ext/odbc/tests/fetches.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$odbc$tests$fetches_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/odbc/tests/fetches.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$odbc$tests$fetches_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_directdsn __attribute__((__unused__)) = (variables != gVariables) ? variables->get("directdsn") : g->GV(directdsn);
  Variant &v_db_user __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_user") : g->GV(db_user);
  Variant &v_db_pass __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_pass") : g->GV(db_pass);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_rh __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rh") : g->GV(rh);
  Variant &v_rv __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rv") : g->GV(rv);
  Variant &v_val __attribute__((__unused__)) = (variables != gVariables) ? variables->get("val") : g->GV(val);
  Variant &v_rval __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rval") : g->GV(rval);
  Variant &v_ar __attribute__((__unused__)) = (variables != gVariables) ? variables->get("ar") : g->GV(ar);

  LINE(3,include("connect.inc", false, variables, "roadsend/pcc/runtime/ext/odbc/tests/"));
  (v_r = LINE(5,invoke_failed("odbc_connect", Array(ArrayInit(3).set(0, ref(v_directdsn)).set(1, ref(v_db_user)).set(2, ref(v_db_pass)).create()), 0x00000000FE9537A1LL)));
  echo(LINE(7,(assignCallTemp(eo_1, toString(x_is_resource(v_r))),concat3("resource\? ", eo_1, "\n"))));
  if (!(toBoolean(v_r))) {
    echo(toString(LINE(9,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  (v_rh = LINE(13,invoke_failed("odbc_exec", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "SELECT * FROM my_table").create()), 0x00000000B7FC229FLL)));
  if (!(toBoolean(v_rh))) {
    echo("odbc_exec failed!\n");
    echo(toString(LINE(16,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    echo(toString(LINE(17,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
    f_exit(1LL);
  }
  echo("testing odbc_fetch_row, next\n");
  (v_rv = LINE(23,invoke_failed("odbc_fetch_row", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x00000000EBCECC34LL)));
  LINE(24,x_var_dump(1, v_rv));
  (v_val = LINE(25,invoke_failed("odbc_result", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 1LL).create()), 0x00000000C27C9E4BLL)));
  LINE(26,x_var_dump(1, v_val));
  echo("testing odbc_fetch_row with row specified\n");
  (v_rval = LINE(30,invoke_failed("odbc_fetch_row", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 2LL).create()), 0x00000000EBCECC34LL)));
  LINE(31,x_var_dump(1, v_rval));
  (v_val = LINE(32,invoke_failed("odbc_result", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 1LL).create()), 0x00000000C27C9E4BLL)));
  LINE(33,x_var_dump(1, v_val));
  echo("testing odbc_fetch_array, next\n");
  (v_rval = LINE(37,invoke_failed("odbc_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000005F90250LL)));
  LINE(38,x_var_dump(1, v_rval));
  echo("testing odbc_fetch_array with row specified\n");
  (v_rval = LINE(42,invoke_failed("odbc_fetch_array", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 4LL).create()), 0x0000000005F90250LL)));
  LINE(43,x_var_dump(1, v_rval));
  echo("testing odbc_fetch_object, next\n");
  (v_rval = LINE(47,invoke_failed("odbc_fetch_object", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x000000001A564A74LL)));
  LINE(48,x_var_dump(1, v_rval));
  echo("testing odbc_fetch_object with row specified\n");
  (v_rval = LINE(52,invoke_failed("odbc_fetch_object", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 4LL).create()), 0x000000001A564A74LL)));
  LINE(53,x_var_dump(1, v_rval));
  (v_ar = ScalarArrays::sa_[0]);
  echo("testing odbc_fetch_into, next\n");
  (v_rval = LINE(58,invoke_failed("odbc_fetch_into", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, ref(v_ar)).create()), 0x000000009CB87A19LL)));
  LINE(59,x_var_dump(1, v_rval));
  LINE(60,x_var_dump(1, v_ar));
  echo("testing odbc_fetch_into with row specified\n");
  (v_rval = LINE(64,invoke_failed("odbc_fetch_into", Array(ArrayInit(3).set(0, ref(v_rh)).set(1, ref(v_ar)).set(2, 7LL).create()), 0x000000009CB87A19LL)));
  LINE(65,x_var_dump(1, v_rval));
  LINE(66,x_var_dump(1, v_ar));
  echo(toString(LINE(68,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
