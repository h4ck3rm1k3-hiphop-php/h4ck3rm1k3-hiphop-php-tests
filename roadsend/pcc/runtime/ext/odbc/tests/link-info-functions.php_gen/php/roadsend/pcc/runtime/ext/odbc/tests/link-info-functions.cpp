
#include <php/roadsend/pcc/runtime/ext/odbc/tests/link-info-functions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$odbc$tests$link_info_functions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/odbc/tests/link-info-functions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$odbc$tests$link_info_functions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_directdsn __attribute__((__unused__)) = (variables != gVariables) ? variables->get("directdsn") : g->GV(directdsn);
  Variant &v_db_user __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_user") : g->GV(db_user);
  Variant &v_db_pass __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_pass") : g->GV(db_pass);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_rh __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rh") : g->GV(rh);
  Variant &v_rr __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rr") : g->GV(rr);

  LINE(3,include("connect.inc", false, variables, "roadsend/pcc/runtime/ext/odbc/tests/"));
  (v_r = LINE(5,invoke_failed("odbc_connect", Array(ArrayInit(3).set(0, ref(v_directdsn)).set(1, ref(v_db_user)).set(2, ref(v_db_pass)).create()), 0x00000000FE9537A1LL)));
  echo(LINE(7,(assignCallTemp(eo_1, toString(x_is_resource(v_r))),concat3("resource\? ", eo_1, "\n"))));
  if (!(toBoolean(v_r))) {
    echo(toString(LINE(9,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  (v_rh = LINE(15,invoke_failed("odbc_columnprivileges", Array(ArrayInit(5).set(0, ref(v_r)).set(1, "").set(2, "test").set(3, "my_table").set(4, "%").create()), 0x00000000A9AB7882LL)));
  echo(LINE(17,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("resource\? ", eo_1, "\n"))));
  if (equal(v_rh, null)) {
    echo(toString(LINE(19,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  LOOP_COUNTER(1);
  {
    while (toBoolean((v_rr = LINE(22,invoke_failed("odbc_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000005F90250LL))))) {
      LOOP_COUNTER_CHECK(1);
      {
        LINE(23,x_var_dump(1, v_rr));
      }
    }
  }
  (v_rh = LINE(27,invoke_failed("odbc_columns", Array(ArrayInit(5).set(0, ref(v_r)).set(1, "").set(2, "test").set(3, "my_table").set(4, "%").create()), 0x0000000018665453LL)));
  echo(LINE(29,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("resource\? ", eo_1, "\n"))));
  if (equal(v_rh, null)) {
    echo(toString(LINE(31,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  LOOP_COUNTER(2);
  {
    while (toBoolean((v_rr = LINE(34,invoke_failed("odbc_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000005F90250LL))))) {
      LOOP_COUNTER_CHECK(2);
      {
        if (equal(v_rr.rvalAt("TYPE_NAME", 0x44433F04EF2F47EFLL), "datetime")) {
          v_rr.set("DATA_TYPE", ("(hack)"), 0x1B94BA6CD0C11AFALL);
          v_rr.set("SQL_DATA_TYPE", ("(hack)"), 0x0273DB35D3F213CCLL);
        }
        LINE(41,x_var_dump(1, v_rr));
      }
    }
  }
  (v_rh = LINE(45,invoke_failed("odbc_foreignkeys", Array(ArrayInit(7).set(0, ref(v_r)).set(1, "").set(2, "").set(3, "").set(4, "").set(5, "").set(6, "").create()), 0x00000000D185566FLL)));
  echo(LINE(47,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("resource\? ", eo_1, "\n"))));
  if (equal(v_rh, null)) {
    echo(toString(LINE(49,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
  }
  else {
    LOOP_COUNTER(3);
    {
      while (toBoolean((v_rr = LINE(52,invoke_failed("odbc_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000005F90250LL))))) {
        LOOP_COUNTER_CHECK(3);
        {
          LINE(53,x_var_dump(1, v_rr));
        }
      }
    }
  }
  (v_rh = LINE(58,invoke_failed("odbc_primarykeys", Array(ArrayInit(4).set(0, ref(v_r)).set(1, "").set(2, "test").set(3, "my_table").create()), 0x0000000056EE5F0BLL)));
  echo(LINE(60,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("resource\? ", eo_1, "\n"))));
  if (equal(v_rh, null)) {
    echo(toString(LINE(62,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
  }
  else {
    LOOP_COUNTER(4);
    {
      while (toBoolean((v_rr = LINE(65,invoke_failed("odbc_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000005F90250LL))))) {
        LOOP_COUNTER_CHECK(4);
        {
          LINE(66,x_var_dump(1, v_rr));
        }
      }
    }
  }
  (v_rh = LINE(72,invoke_failed("odbc_tables", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x0000000088E1C764LL)));
  echo(LINE(74,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("resource\? ", eo_1, "\n"))));
  if (equal(v_rh, null)) {
    echo(toString(LINE(76,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
  }
  else {
    LOOP_COUNTER(5);
    {
      while (toBoolean((v_rr = LINE(79,invoke_failed("odbc_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000005F90250LL))))) {
        LOOP_COUNTER_CHECK(5);
        {
          LINE(80,x_var_dump(1, v_rr));
        }
      }
    }
  }
  (v_rh = LINE(85,invoke_failed("odbc_tableprivileges", Array(ArrayInit(4).set(0, ref(v_r)).set(1, "").set(2, "test").set(3, "%").create()), 0x0000000097637396LL)));
  echo(LINE(87,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("resource\? ", eo_1, "\n"))));
  if (equal(v_rh, null)) {
    echo(toString(LINE(89,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
  }
  else {
    LOOP_COUNTER(6);
    {
      while (toBoolean((v_rr = LINE(92,invoke_failed("odbc_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000005F90250LL))))) {
        LOOP_COUNTER_CHECK(6);
        {
          LINE(93,x_var_dump(1, v_rr));
        }
      }
    }
  }
  (v_rh = LINE(98,invoke_failed("odbc_statistics", Array(ArrayInit(6).set(0, ref(v_r)).set(1, "").set(2, "test").set(3, "my_table").set(4, "").set(5, "").create()), 0x00000000C1FA7C6FLL)));
  echo(LINE(100,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("resource\? ", eo_1, "\n"))));
  if (equal(v_rh, null)) {
    echo(toString(LINE(102,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
  }
  else {
    LOOP_COUNTER(7);
    {
      while (toBoolean((v_rr = LINE(105,invoke_failed("odbc_fetch_array", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000005F90250LL))))) {
        LOOP_COUNTER_CHECK(7);
        {
          LINE(106,x_var_dump(1, v_rr));
        }
      }
    }
  }
  echo(toString(LINE(124,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
