
#ifndef __GENERATED_php_roadsend_pcc_runtime_ext_odbc_tests_exec_h__
#define __GENERATED_php_roadsend_pcc_runtime_ext_odbc_tests_exec_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/ext/odbc/tests/exec.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$ext$odbc$tests$exec_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_ext_odbc_tests_exec_h__
