
#include <php/roadsend/pcc/runtime/ext/odbc/tests/resultoptions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$odbc$tests$resultoptions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/odbc/tests/resultoptions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$odbc$tests$resultoptions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_directdsn __attribute__((__unused__)) = (variables != gVariables) ? variables->get("directdsn") : g->GV(directdsn);
  Variant &v_db_user __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_user") : g->GV(db_user);
  Variant &v_db_pass __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_pass") : g->GV(db_pass);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_rh __attribute__((__unused__)) = (variables != gVariables) ? variables->get("rh") : g->GV(rh);

  LINE(3,include("connect.inc", false, variables, "roadsend/pcc/runtime/ext/odbc/tests/"));
  (v_r = LINE(5,invoke_failed("odbc_connect", Array(ArrayInit(3).set(0, ref(v_directdsn)).set(1, ref(v_db_user)).set(2, ref(v_db_pass)).create()), 0x00000000FE9537A1LL)));
  echo(LINE(7,(assignCallTemp(eo_1, toString(x_is_resource(v_r))),concat3("resource\? ", eo_1, "\n"))));
  if (!(toBoolean(v_r))) {
    echo(toString(LINE(9,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  (v_rh = LINE(14,invoke_failed("odbc_exec", Array(ArrayInit(2).set(0, ref(v_r)).set(1, "SELECT * FROM my_table").create()), 0x00000000B7FC229FLL)));
  if (!(toBoolean(v_rh))) {
    echo("odbc_exec failed!\n");
    echo(toString(LINE(17,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    echo(toString(LINE(18,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
    f_exit(1LL);
  }
  echo(LINE(22,(assignCallTemp(eo_1, toString(x_is_resource(v_rh))),concat3("resource\? ", eo_1, "\n"))));
  echo(concat(toString(LINE(25,invoke_failed("odbc_cursor", Array(ArrayInit(1).set(0, ref(v_rh)).create()), 0x0000000012499649LL))), "\n"));
  echo(concat(toString(LINE(28,invoke_failed("odbc_field_len", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 1LL).create()), 0x000000005343E7FDLL))), "\n"));
  echo(concat(toString(LINE(29,invoke_failed("odbc_field_precision", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 2LL).create()), 0x0000000002568234LL))), "\n"));
  echo(concat(toString(LINE(30,invoke_failed("odbc_field_scale", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 3LL).create()), 0x0000000078E22C02LL))), "\n"));
  echo(concat(toString(LINE(32,invoke_failed("odbc_field_name", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 2LL).create()), 0x000000003981E6E1LL))), "\n"));
  echo(concat(toString(LINE(33,invoke_failed("odbc_field_num", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, "field2").create()), 0x000000009D1977BELL))), "\n"));
  echo(concat(toString(LINE(35,invoke_failed("odbc_field_type", Array(ArrayInit(2).set(0, ref(v_rh)).set(1, 3LL).create()), 0x000000004D0E9A9DLL))), "\n"));
  echo(toString(LINE(38,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
