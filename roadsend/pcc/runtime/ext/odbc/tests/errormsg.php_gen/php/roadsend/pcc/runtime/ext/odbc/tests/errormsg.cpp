
#include <php/roadsend/pcc/runtime/ext/odbc/tests/errormsg.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$odbc$tests$errormsg_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/odbc/tests/errormsg.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$odbc$tests$errormsg_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);

  (v_r = LINE(3,invoke_failed("odbc_connect", Array(ArrayInit(3).set(0, "fake").set(1, 1LL).set(2, 2LL).create()), 0x00000000FE9537A1LL)));
  echo(LINE(5,(assignCallTemp(eo_1, toString(x_is_resource(v_r))),concat3("resource\? ", eo_1, "\n"))));
  echo("resource error msg:\n");
  echo(concat(toString(LINE(7,invoke_failed("odbc_errormsg", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000006E03A743LL))), "\n"));
  echo("resource state:\n");
  echo(concat(toString(LINE(9,invoke_failed("odbc_error", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000FD245312LL))), "\n"));
  echo("global error msg:\n");
  echo(concat(toString(LINE(12,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))), "\n"));
  echo("global state:\n");
  echo(concat(toString(LINE(14,invoke_failed("odbc_error", Array(), 0x00000000FD245312LL))), "\n"));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
