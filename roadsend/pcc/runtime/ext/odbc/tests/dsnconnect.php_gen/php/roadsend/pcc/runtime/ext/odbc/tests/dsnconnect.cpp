
#include <php/roadsend/pcc/runtime/ext/odbc/tests/dsnconnect.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$odbc$tests$dsnconnect_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/odbc/tests/dsnconnect.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$odbc$tests$dsnconnect_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_dsn __attribute__((__unused__)) = (variables != gVariables) ? variables->get("dsn") : g->GV(dsn);
  Variant &v_db_user __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_user") : g->GV(db_user);
  Variant &v_db_pass __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_pass") : g->GV(db_pass);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);

  LINE(3,include("connect.inc", false, variables, "roadsend/pcc/runtime/ext/odbc/tests/"));
  (v_r = LINE(5,invoke_failed("odbc_connect", Array(ArrayInit(3).set(0, ref(v_dsn)).set(1, ref(v_db_user)).set(2, ref(v_db_pass)).create()), 0x00000000FE9537A1LL)));
  echo(LINE(7,(assignCallTemp(eo_1, toString(x_is_resource(v_r))),concat3("resource\? ", eo_1, "\n"))));
  if (!(toBoolean(v_r))) {
    echo(toString(LINE(9,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  echo(toString(LINE(13,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
  echo("\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
