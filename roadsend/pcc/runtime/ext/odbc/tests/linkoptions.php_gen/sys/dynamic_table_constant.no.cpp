
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>


using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

extern const StaticString k_SQL_FETCH_FIRST;
extern const StaticString k_SQL_FETCH_NEXT;


// Get Constant Table
Variant get_constant(CStrRef name) {
  DECLARE_GLOBAL_VARIABLES(g);
  const char* s = name.data();
  int64 hash = hash_string(s);
  switch (hash & 3) {
    case 0:
      HASH_RETURN(0x7832C472AC1858B4LL, k_SQL_FETCH_FIRST, SQL_FETCH_FIRST);
      break;
    case 1:
      HASH_RETURN(0x6D9716DCE7CD6589LL, k_SQL_FETCH_NEXT, SQL_FETCH_NEXT);
      break;
    default:
      break;
  }
  return get_builtin_constant(name);
}

///////////////////////////////////////////////////////////////////////////////
}
