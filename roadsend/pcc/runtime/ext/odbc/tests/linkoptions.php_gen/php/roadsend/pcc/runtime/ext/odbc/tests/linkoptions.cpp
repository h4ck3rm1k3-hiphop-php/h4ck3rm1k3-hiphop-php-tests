
#include <php/roadsend/pcc/runtime/ext/odbc/tests/linkoptions.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

const StaticString k_SQL_FETCH_FIRST = "SQL_FETCH_FIRST";
const StaticString k_SQL_FETCH_NEXT = "SQL_FETCH_NEXT";

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$ext$odbc$tests$linkoptions_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/ext/odbc/tests/linkoptions.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$ext$odbc$tests$linkoptions_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  Variant eo_0;
  Variant eo_1;
  Variant eo_2;
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  Variant &v_directdsn __attribute__((__unused__)) = (variables != gVariables) ? variables->get("directdsn") : g->GV(directdsn);
  Variant &v_db_user __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_user") : g->GV(db_user);
  Variant &v_db_pass __attribute__((__unused__)) = (variables != gVariables) ? variables->get("db_pass") : g->GV(db_pass);
  Variant &v_r __attribute__((__unused__)) = (variables != gVariables) ? variables->get("r") : g->GV(r);
  Variant &v_v __attribute__((__unused__)) = (variables != gVariables) ? variables->get("v") : g->GV(v);

  LINE(3,include("connect.inc", false, variables, "roadsend/pcc/runtime/ext/odbc/tests/"));
  (v_r = LINE(5,invoke_failed("odbc_connect", Array(ArrayInit(3).set(0, ref(v_directdsn)).set(1, ref(v_db_user)).set(2, ref(v_db_pass)).create()), 0x00000000FE9537A1LL)));
  echo(LINE(7,(assignCallTemp(eo_1, toString(x_is_resource(v_r))),concat3("resource\? ", eo_1, "\n"))));
  if (!(toBoolean(v_r))) {
    echo(toString(LINE(9,invoke_failed("odbc_errormsg", Array(), 0x000000006E03A743LL))));
    f_exit(1LL);
  }
  (v_v = LINE(15,invoke_failed("odbc_autocommit", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000ED7EE5AALL)));
  LINE(16,x_var_dump(1, v_v));
  (v_v = LINE(17,invoke_failed("odbc_autocommit", Array(ArrayInit(2).set(0, ref(v_r)).set(1, 1LL).create()), 0x00000000ED7EE5AALL)));
  LINE(18,x_var_dump(1, v_v));
  (v_v = LINE(19,invoke_failed("odbc_autocommit", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000ED7EE5AALL)));
  LINE(20,x_var_dump(1, v_v));
  (v_v = LINE(21,invoke_failed("odbc_autocommit", Array(ArrayInit(2).set(0, ref(v_r)).set(1, 0LL).create()), 0x00000000ED7EE5AALL)));
  LINE(22,x_var_dump(1, v_v));
  (v_v = LINE(23,invoke_failed("odbc_autocommit", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x00000000ED7EE5AALL)));
  LINE(24,x_var_dump(1, v_v));
  (v_v = LINE(27,invoke_failed("odbc_data_source", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQL_FETCH_FIRST).create()), 0x0000000051A01269LL)));
  LINE(28,x_var_dump(1, v_v));
  (v_v = LINE(29,invoke_failed("odbc_data_source", Array(ArrayInit(2).set(0, ref(v_r)).set(1, k_SQL_FETCH_NEXT).create()), 0x0000000051A01269LL)));
  LINE(30,x_var_dump(1, v_v));
  echo(toString(LINE(32,invoke_failed("odbc_close", Array(ArrayInit(1).set(0, ref(v_r)).create()), 0x000000008F78E82CLL))));
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
