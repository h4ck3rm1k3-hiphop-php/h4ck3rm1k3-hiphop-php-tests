
#ifndef __GENERATED_php_roadsend_pcc_runtime_php_ext_pdo_tests_basic_h__
#define __GENERATED_php_roadsend_pcc_runtime_php_ext_pdo_tests_basic_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/runtime/php-ext/pdo/tests/basic.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

Variant pm_php$roadsend$pcc$runtime$php_ext$pdo$tests$basic_php(bool incOnce = false, LVariableTable* variables = NULL);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_runtime_php_ext_pdo_tests_basic_h__
