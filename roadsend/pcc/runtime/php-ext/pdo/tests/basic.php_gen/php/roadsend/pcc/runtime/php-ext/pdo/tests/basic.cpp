
#include <php/roadsend/pcc/runtime/php-ext/pdo/tests/basic.h>
#include <cpp/ext/ext.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

/* preface starts */
/* preface finishes */
Variant pm_php$roadsend$pcc$runtime$php_ext$pdo$tests$basic_php(bool incOnce /* = false */, LVariableTable* variables /* = NULL */) {
  FUNCTION_INJECTION(run_init::roadsend/pcc/runtime/php-ext/pdo/tests/basic.php);
  {
    DECLARE_GLOBAL_VARIABLES(g);
    bool &alreadyRun = g->run_pm_php$roadsend$pcc$runtime$php_ext$pdo$tests$basic_php;
    if (alreadyRun) { if (incOnce) return true;}
    else alreadyRun = true;
    if (!variables) variables = g;
  }
  DECLARE_GLOBAL_VARIABLES(g);
  LVariableTable *gVariables __attribute__((__unused__)) = get_variable_table();
  echo(concat((toString((LINE(3,x_extension_loaded("pdo"))) ? (("PDO extension is loaded")) : (("no PDO extension")))), "\n"));
  echo("no PDO class\n");
  echo("no PDOStatement class\n");
  echo("done\n");
  return true;
} /* function */

///////////////////////////////////////////////////////////////////////////////
}
