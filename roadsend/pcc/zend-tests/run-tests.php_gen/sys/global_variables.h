
#ifndef __GENERATED_sys_global_variables_h__
#define __GENERATED_sys_global_variables_h__

#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class Forward Declarations

class GlobalVariables : public SystemGlobals {
DECLARE_SMART_ALLOCATION_NOCALLBACKS(GlobalVariables);
public:
  GlobalVariables();
  ~GlobalVariables();
  static GlobalVariables *Create() { return NEW(GlobalVariables)(); }
  static void Delete(GlobalVariables *p) { DELETE(GlobalVariables)(p); }
  static void initialize();

  bool dummy; // for easier constructor initializer output

  // Global Variables
  BEGIN_GVS()
    GVS(CUR_DIR)
    GVS(cwd)
    GVS(environment)
    GVS(php)
    GVS(php_cgi)
    GVS(log_format)
    GVS(DETAILED)
    GVS(user_tests)
    GVS(exts_to_test)
    GVS(ini_overwrites)
    GVS(php_info)
    GVS(pass_options)
    GVS(test_files)
    GVS(redir_tests)
    GVS(test_results)
    GVS(PHP_FAILED_TESTS)
    GVS(failed_tests_file)
    GVS(pass_option_n)
    GVS(compression)
    GVS(output_file)
    GVS(just_save_results)
    GVS(leak_check)
    GVS(html_output)
    GVS(html_file)
    GVS(temp_source)
    GVS(temp_target)
    GVS(temp_urlbase)
    GVS(conf_passed)
    GVS(no_clean)
    GVS(cfgtypes)
    GVS(cfgfiles)
    GVS(cfg)
    GVS(type)
    GVS(file)
    GVS(i)
    GVS(is_switch)
    GVS(switch)
    GVS(repeat)
    GVS(test_list)
    GVS(test)
    GVS(matches)
    GVS(testfile)
    GVS(test_cnt)
    GVS(start_time)
    GVS(test_idx)
    GVS(end_time)
    GVS(exts_tested)
    GVS(exts_skipped)
    GVS(ignored_by_ext)
    GVS(test_dirs)
    GVS(optionals)
    GVS(dir)
    GVS(key)
    GVS(val)
    GVS(sum_results)
    GVS(info_params)
    GVS(IN_REDIRECT)
    GVS(n_total)
    GVS(percent_results)
    GVS(failed_test_summary)
  END_GVS(60)

  // Dynamic Constants
  Variant k_TESTED_PHP_VERSION;

  // Function/Method Static Variables

  // Function/Method Static Variable Initialization Booleans

  // Class Static Variables

  // Class Static Initializer Flags

  // PseudoMain Variables
  bool run_pm_php$roadsend$pcc$zend_tests$run_tests_php;

  // Redeclared Functions

  // Redeclared Classes

  // Global Array Wrapper Methods
  virtual ssize_t staticSize() const {
    return 72;
  }

  // LVariableTable Methods
  virtual CVarRef getRefByIdx(ssize_t idx, Variant &k);
  virtual ssize_t getIndex(const char *s, int64 prehash) const;
  virtual Variant &getImpl(CStrRef s, int64 hash);
  virtual bool exists(const char *s, int64 hash = -1) const;
};

// Scalar Arrays
class ScalarArrays : public SystemScalarArrays {
public:
  static void initialize();

  static StaticArray sa_[12];
};


LVariableTable *get_variable_table();

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_sys_global_variables_h__
