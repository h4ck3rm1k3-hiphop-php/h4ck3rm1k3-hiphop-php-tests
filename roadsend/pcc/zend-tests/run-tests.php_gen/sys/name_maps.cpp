
#include <cpp/base/hphp.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////


// Class -> File
const char *g_source_cls2file[] = {
  NULL
};

// Function -> File
const char *g_source_func2file[] = {
  "comp_line", "roadsend/pcc/zend-tests/run-tests.php",
  "compute_summary", "roadsend/pcc/zend-tests/run-tests.php",
  "count_array_diff", "roadsend/pcc/zend-tests/run-tests.php",
  "error", "roadsend/pcc/zend-tests/run-tests.php",
  "error_report", "roadsend/pcc/zend-tests/run-tests.php",
  "find_files", "roadsend/pcc/zend-tests/run-tests.php",
  "generate_array_diff", "roadsend/pcc/zend-tests/run-tests.php",
  "generate_diff", "roadsend/pcc/zend-tests/run-tests.php",
  "get_summary", "roadsend/pcc/zend-tests/run-tests.php",
  "mail_qa_team", "roadsend/pcc/zend-tests/run-tests.php",
  "run_all_tests", "roadsend/pcc/zend-tests/run-tests.php",
  "run_test", "roadsend/pcc/zend-tests/run-tests.php",
  "save_text", "roadsend/pcc/zend-tests/run-tests.php",
  "settings2array", "roadsend/pcc/zend-tests/run-tests.php",
  "settings2params", "roadsend/pcc/zend-tests/run-tests.php",
  "show_end", "roadsend/pcc/zend-tests/run-tests.php",
  "show_redirect_ends", "roadsend/pcc/zend-tests/run-tests.php",
  "show_redirect_start", "roadsend/pcc/zend-tests/run-tests.php",
  "show_result", "roadsend/pcc/zend-tests/run-tests.php",
  "show_start", "roadsend/pcc/zend-tests/run-tests.php",
  "show_summary", "roadsend/pcc/zend-tests/run-tests.php",
  "show_test", "roadsend/pcc/zend-tests/run-tests.php",
  "system_with_timeout", "roadsend/pcc/zend-tests/run-tests.php",
  "test_name", "roadsend/pcc/zend-tests/run-tests.php",
  "test_sort", "roadsend/pcc/zend-tests/run-tests.php",
  "write_information", "roadsend/pcc/zend-tests/run-tests.php",
  NULL
};

// Param RTTI Id -> Name
const char *g_paramrtti_map[] = {
  NULL
};

///////////////////////////////////////////////////////////////////////////////
}
