
#include <cpp/base/hphp.h>
#include <sys/global_variables.h>

using namespace std;

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

ssize_t GlobalVariables::getIndex(const char* s, int64 prehash) const {
  DECLARE_GLOBAL_VARIABLES(g);
  int64 hash = hash_string(s);
  switch (hash & 255) {
    case 1:
      HASH_INDEX(0x50645ABB5EE07801LL, _POST, 4);
      break;
    case 3:
      HASH_INDEX(0x6649184C41356B03LL, HTTP_RAW_POST_DATA, 10);
      HASH_INDEX(0x11A5375142A75D03LL, http_response_header, 11);
      break;
    case 5:
      HASH_INDEX(0x178B5DB860BB1F05LL, n_total, 69);
      break;
    case 10:
      HASH_INDEX(0x508FC7C8724A760ALL, type, 44);
      break;
    case 13:
      HASH_INDEX(0x262F35FB33EC540DLL, ini_overwrites, 21);
      break;
    case 14:
      HASH_INDEX(0x516FBD36FC674A0ELL, _FILES, 6);
      break;
    case 18:
      HASH_INDEX(0x152FE5CE75DA9C12LL, failed_tests_file, 28);
      break;
    case 19:
      HASH_INDEX(0x596A642EB89EED13LL, argc, 0);
      HASH_INDEX(0x3A1DC49AA6CFEA13LL, test, 51);
      break;
    case 22:
      HASH_INDEX(0x6B3093D1F2F27D16LL, cwd, 13);
      break;
    case 28:
      HASH_INDEX(0x1F09B43053EBEB1CLL, log_format, 17);
      break;
    case 29:
      HASH_INDEX(0x72C2CA3ECF7C191DLL, test_dirs, 61);
      break;
    case 37:
      HASH_INDEX(0x09EF6630A291F025LL, environment, 14);
      break;
    case 44:
      HASH_INDEX(0x126C26F0F6A58E2CLL, cfgtypes, 41);
      break;
    case 45:
      HASH_INDEX(0x1B723EAC6938822DLL, end_time, 57);
      break;
    case 54:
      HASH_INDEX(0x24CB2D71EFF85036LL, is_switch, 47);
      break;
    case 55:
      HASH_INDEX(0x310559E2D6E6EC37LL, user_tests, 19);
      break;
    case 59:
      HASH_INDEX(0x4A55D1778C725E3BLL, compression, 30);
      break;
    case 60:
      HASH_INDEX(0x43365B62E067203CLL, temp_urlbase, 38);
      break;
    case 61:
      HASH_INDEX(0x6DE3CFB71905593DLL, test_files, 24);
      break;
    case 62:
      HASH_INDEX(0x7D8D2E5AC1F9AC3ELL, html_file, 35);
      break;
    case 63:
      HASH_INDEX(0x1F878FB806A18D3FLL, _REQUEST, 8);
      break;
    case 65:
      HASH_INDEX(0x52B182B0A94E9C41LL, php, 15);
      break;
    case 73:
      HASH_INDEX(0x0759FB4517508949LL, _GET, 3);
      break;
    case 91:
      HASH_INDEX(0x612E37678CE7DB5BLL, file, 45);
      break;
    case 101:
      HASH_INDEX(0x687BED2B0019D265LL, start_time, 55);
      break;
    case 104:
      HASH_INDEX(0x77709D492BF62268LL, temp_source, 36);
      break;
    case 105:
      HASH_INDEX(0x51DA1403EA629E69LL, info_params, 67);
      break;
    case 108:
      HASH_INDEX(0x45EC4BFB60499F6CLL, conf_passed, 39);
      break;
    case 111:
      HASH_INDEX(0x6511131491A84B6FLL, DETAILED, 18);
      break;
    case 118:
      HASH_INDEX(0x33885EEC4F612376LL, html_output, 34);
      HASH_INDEX(0x1042147977F84376LL, optionals, 62);
      break;
    case 119:
      HASH_INDEX(0x400AC3BA82543C77LL, failed_test_summary, 71);
      break;
    case 121:
      HASH_INDEX(0x07627BF26E5ADC79LL, leak_check, 33);
      break;
    case 130:
      HASH_INDEX(0x4056C2E766E0B782LL, val, 65);
      break;
    case 131:
      HASH_INDEX(0x4B9AE5064B0A5283LL, exts_to_test, 20);
      break;
    case 133:
      HASH_INDEX(0x124AB4DF0D760A85LL, pass_options, 23);
      break;
    case 135:
      HASH_INDEX(0x612DD31212E90587LL, key, 64);
      break;
    case 137:
      HASH_INDEX(0x1E2E626C9AD36E89LL, PHP_FAILED_TESTS, 27);
      break;
    case 138:
      HASH_INDEX(0x2D1866296FE4BE8ALL, cfgfiles, 42);
      break;
    case 144:
      HASH_INDEX(0x7320B4E3FF243290LL, _ENV, 7);
      HASH_INDEX(0x67FECFEAB01DEA90LL, percent_results, 70);
      break;
    case 151:
      HASH_INDEX(0x72CCC88F90317797LL, exts_tested, 58);
      break;
    case 152:
      HASH_INDEX(0x0EB22EDA95766E98LL, i, 46);
      break;
    case 163:
      HASH_INDEX(0x3760929554A51DA3LL, _COOKIE, 5);
      break;
    case 179:
      HASH_INDEX(0x4B078B47A2EF86B3LL, CUR_DIR, 12);
      break;
    case 180:
      HASH_INDEX(0x3AD6EDBE00AC3BB4LL, php_info, 22);
      break;
    case 182:
      HASH_INDEX(0x31CF71EAC03B86B6LL, testfile, 53);
      break;
    case 183:
      HASH_INDEX(0x15B870ABDE28C8B7LL, php_cgi, 16);
      break;
    case 185:
      HASH_INDEX(0x61CFEEED54DD0AB9LL, matches, 52);
      break;
    case 186:
      HASH_INDEX(0x39D40BAF3B9E07BALL, test_idx, 56);
      break;
    case 195:
      HASH_INDEX(0x346B87C4FC1A7AC3LL, no_clean, 40);
      break;
    case 196:
      HASH_INDEX(0x7E7EB0DE39E966C4LL, sum_results, 66);
      break;
    case 198:
      HASH_INDEX(0x10EA7DC57768F8C6LL, argv, 1);
      HASH_INDEX(0x2D2DC4D373BA0DC6LL, cfg, 43);
      HASH_INDEX(0x77EE85A013BFFCC6LL, ignored_by_ext, 60);
      break;
    case 200:
      HASH_INDEX(0x5D2AE074B13A41C8LL, pass_option_n, 29);
      break;
    case 203:
      HASH_INDEX(0x7EDA455F36DBDCCBLL, just_save_results, 32);
      break;
    case 206:
      HASH_INDEX(0x70BCE0E305461CCELL, switch, 48);
      break;
    case 207:
      HASH_INDEX(0x522821F5063592CFLL, dir, 63);
      HASH_INDEX(0x59B4E66CC0580FCFLL, IN_REDIRECT, 68);
      break;
    case 209:
      HASH_INDEX(0x29DFC3A6DC027BD1LL, _SESSION, 9);
      HASH_INDEX(0x318FBA9639C18FD1LL, temp_target, 37);
      HASH_INDEX(0x2A28A6C4E9876ED1LL, test_list, 50);
      break;
    case 219:
      HASH_INDEX(0x4CE40227CDA7D4DBLL, redir_tests, 25);
      break;
    case 222:
      HASH_INDEX(0x7932B72E7B0466DELL, exts_skipped, 59);
      break;
    case 235:
      HASH_INDEX(0x519638790DED9BEBLL, test_cnt, 54);
      break;
    case 236:
      HASH_INDEX(0x3A944C0BE05000ECLL, output_file, 31);
      break;
    case 238:
      HASH_INDEX(0x14297F74B68B58EELL, _SERVER, 2);
      break;
    case 254:
      HASH_INDEX(0x5154C50FC7F55FFELL, test_results, 26);
      HASH_INDEX(0x5A5A8C53402EACFELL, repeat, 49);
      break;
    default:
      break;
  }
  return m_px ? (m_px->getIndex(s, prehash) + 72) : -1;
}

///////////////////////////////////////////////////////////////////////////////
}
