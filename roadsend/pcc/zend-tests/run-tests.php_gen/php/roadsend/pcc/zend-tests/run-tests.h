
#ifndef __GENERATED_php_roadsend_pcc_zend_tests_run_tests_h__
#define __GENERATED_php_roadsend_pcc_zend_tests_run_tests_h__

#include <cpp/base/hphp.h>
#include <sys/global_variables.h>
#include <php/roadsend/pcc/zend-tests/run-tests.fw.h>

// Declarations

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

void f_show_summary();
String f_run_test(Variant v_php, Variant v_file, Variant v_env);
void f_settings2params(Variant v_ini_settings);
Variant f_test_name(CVarRef v_name);
void f_error(CStrRef v_message);
void f_show_result(CVarRef v_result, Variant v_tested, CVarRef v_tested_file, Variant v_extra = "", CVarRef v_temp_filenames = null_variant);
void f_show_redirect_ends(CVarRef v_tests, CVarRef v_tested, CVarRef v_tested_file);
void f_show_start(CVarRef v_start_time);
void f_error_report(Variant v_testname, Variant v_logname, CVarRef v_tested);
void f_run_all_tests(CVarRef v_test_files, CVarRef v_env, CVarRef v_redir_tested = null_variant);
void f_compute_summary();
Array f_generate_array_diff(CArrRef v_ar1, CArrRef v_ar2, bool v_is_reg, CArrRef v_w);
Variant f_test_sort(Variant v_a, Variant v_b);
Variant f_system_with_timeout(CVarRef v_commandline, CVarRef v_env = null_variant, CVarRef v_stdin = null_variant);
void f_show_end(CVarRef v_end_time);
void f_find_files(CVarRef v_dir, bool v_is_ext_dir = false, bool v_ignore = false);
int64 f_count_array_diff(CVarRef v_ar1, CVarRef v_ar2, CVarRef v_is_reg, CVarRef v_w, Numeric v_idx1, Numeric v_idx2, int v_cnt1, int v_cnt2, Numeric v_steps);
int64 f_mail_qa_team(CVarRef v_data, CVarRef v_compression, bool v_status = false);
Variant pm_php$roadsend$pcc$zend_tests$run_tests_php(bool incOnce = false, LVariableTable* variables = NULL);
void f_save_text(CVarRef v_filename, CVarRef v_text, CVarRef v_filename_copy = null_variant);
Variant f_get_summary(bool v_show_ext_summary, bool v_show_html);
String f_generate_diff(CVarRef v_wanted, CVarRef v_wanted_re, CVarRef v_output);
void f_show_redirect_start(CVarRef v_tests, CVarRef v_tested, CVarRef v_tested_file);
void f_write_information(CVarRef v_show_html);
void f_show_test(CVarRef v_test_idx, CVarRef v_shortname);
Variant f_comp_line(CVarRef v_l1, CVarRef v_l2, CVarRef v_is_reg);
void f_settings2array(CVarRef v_settings, Variant v_ini_settings);

///////////////////////////////////////////////////////////////////////////////
}

#endif // __GENERATED_php_roadsend_pcc_zend_tests_run_tests_h__
